<?php

namespace App\Http\Controllers;

use App\Http\Classes\CourseClass;
use App\Http\Classes\DepartmentClass;
use App\Http\Classes\LectureCourseClass;
use App\Http\Classes\LecturerClass;
use App\Http\Classes\ReviewClass;
use App\Http\Classes\Utils;
use App\Models\LectureCourseModel;
use App\models\ReviewModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class LecturerController extends Controller
{
    /**
     * @return $this
     */
    public function index(){

        try{

            $courses = DB::table(CourseClass::TABLE_NAME)
                ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name", CourseClass::TABLE_NAME . '.' . CourseClass::ID,
                    LectureCourseClass::TABLE_NAME.'.'.LectureCourseClass::ID.' as lecture_course_id',

                    CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::DESC,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)
                ->join(DepartmentClass::TABLE_NAME, DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                    CourseClass::ID_DEPARTMENT)
                ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE,
                    CourseClass::TABLE_NAME . '.' . CourseClass::ID)
                ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                    LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
            ->where(LecturerClass::TABLE_NAME . '.' . LecturerClass::ID_LOGIN, '=', Utils::getLoginId())
                ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE)
                ->get();

            return view("front.student_courses")
                ->with("courses",$courses);

        }catch (Exception $e){

            return Redirect::back();

        }
    }

    public function listAllCourses()
    {

        $courses = DB::table(CourseClass::TABLE_NAME)
            ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name", CourseClass::TABLE_NAME . '.' . CourseClass::ID,
                LectureCourseClass::TABLE_NAME.'.'.LectureCourseClass::ID.' as lecture_course_id',
                CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::DESC,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)
            ->join(DepartmentClass::TABLE_NAME, DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                CourseClass::ID_DEPARTMENT)
            ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE,
                CourseClass::TABLE_NAME . '.' . CourseClass::ID)
            ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
//            ->where(LecturerClass::TABLE_NAME . '.' . LecturerClass::ID_LOGIN, '=', Utils::getId())
            ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE)
            ->get();

        $listCourse = [];
        foreach ($courses as $course){
            $totRev = ReviewModel::where(ReviewClass::ID_LECT_COURSE, $course->lecture_course_id)
                ->avg(ReviewClass::REVIEW);

            $obj=(object)[
                'course_name'=> $course->course_name,
                'id'=>$course->id,
                'lecture_course_id'=>$course->lecture_course_id,
                'image'=>$course->image,
                'desc'=>$course->desc,
                'title'=>$course->title,
                'name'=>$course->name,
                'surname'=>$course->surname,
                'review' => round($totRev),
            ];
            array_push($listCourse, $obj);
        }
        return view("front.course_list")
            ->with("courses", $listCourse);
    }
}
