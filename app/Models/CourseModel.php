<?php

namespace App\Models;

use App\Http\Classes\CourseClass;
use Illuminate\Database\Eloquent\Model;

class CourseModel extends Model
{
    protected $table = CourseClass::TABLE_NAME;
    protected $primaryKey = CourseClass::ID;
    public $timestamps = true;
}
