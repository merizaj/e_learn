<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 7/7/2018
 * Time: 8:17 PM
 */

namespace App\Http\Classes;


class UserCommentClass
{

    const TABLE_NAME = "stud_comm";
    const ID = "id";
    const COMMENT = "comment";
    const ID_LOGIN= "id_login";
    const ID_LECT_COURSE = "id_lect_course";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}