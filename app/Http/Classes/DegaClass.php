<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 4:17 PM
 */

namespace App\Http\Classes;


class DegaClass
{
    const TABLE_NAME = "dega";
    const ID = "id";
    const NAME = "name";
    const ID_DEPARTMENT = "id_department";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}