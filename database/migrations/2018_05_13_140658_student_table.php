<?php

use App\Http\Classes\StudentClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(StudentClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(StudentClass::ID);
            $table->string(StudentClass::NAME);
            $table->string(StudentClass::SURNAME);
            $table->string(StudentClass::TITLE);
            $table->string(StudentClass::NR_MATRIKULL);
            $table->string(StudentClass::EMAIL)->nullable();
            $table->string(StudentClass::CEL)->nullable();
            $table->string(StudentClass::VITI);
            $table->integer(StudentClass::ID_LOGIN);
            $table->integer(StudentClass::ID_DEPARTMENT);
//            $table->string(StudentClass::ID_DEGA);
            $table->smallInteger(StudentClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
