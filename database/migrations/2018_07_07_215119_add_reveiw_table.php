<?php

use App\Http\Classes\ReviewClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReveiwTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReviewClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(ReviewClass::ID);
            $table->string(ReviewClass::REVIEW);
            $table->integer(ReviewClass::ID_LOGIN);
            $table->integer(ReviewClass::ID_LECT_COURSE);
            $table->smallInteger(ReviewClass::STATUS)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
