<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:04 PM
 */

namespace App\Http\Classes;


class StudentClass
{
    const TABLE_NAME = "student";
    const ID = "id";
    const NAME = "name";
    const SURNAME = "surname";
    const NR_MATRIKULL = "nr_matrikull";
    const EMAIL = "email";
    const CEL = "cel";
    const ID_DEGA = "id_dega";
    const ID_DEPARTMENT = "id_department";
    const TITLE = "title";
    const VITI = "viti";
    const ID_LOGIN = "id_login";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const BACHELOR = 1;
    const MASTER = 2;
}