<?php

namespace App\Models;

use App\Http\Classes\LessonClass;
use Illuminate\Database\Eloquent\Model;

class LessonModel extends Model
{
    protected $table = LessonClass::TABLE_NAME;
    protected $primaryKey = LessonClass::ID;
    public $timestamps = true;

}
