<section class="content-bar">
    <div class="container" >
        <ul id="minimenu">
            @if(\App\Http\Classes\Utils::isStudent())
                <li>
                    <a href="{!! \Illuminate\Support\Facades\URL::route("student_courses") !!}">
                        <i class="icon md-book-1"></i>
                        Learning
                    </a>
                </li>
            @elseif(\App\Http\Classes\Utils::isLecturer())
                <li>
                    <a href="{!! \Illuminate\Support\Facades\URL::route("lecturer_courses") !!}">
                        <i class="icon md-people"></i>
                        Teaching
                    </a>
                </li>
            @endif

            <li >
                <a href="{!! \Illuminate\Support\Facades\URL::route("profile") !!}">
                    <i class="icon md-user-minus"></i>
                    Profile
                </a>
            </li>
        </ul>
    </div>
</section>