<?php

namespace App\Http\Middleware;

use App\Http\Classes\Utils;
use Closure;

class isLecturer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Utils::isLecturer()){
            abort(404);
        }
        return $next($request);
    }
}
