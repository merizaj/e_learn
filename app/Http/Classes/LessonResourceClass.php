<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:05 PM
 */

namespace App\Http\Classes;


class LessonResourceClass
{
    const TABLE_NAME = "lesson_resource";
    const ID = "id";
    const TITLE = "title";
    const TYPE = "type";
    const PATH = "path";
    const LINK = "link";
    const ID_LECTURE_COURSE = "id_lesson";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}