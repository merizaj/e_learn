<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', "CourseController@homeView")->name('home');
Route::get('/about',function (){
    return view("front.about");
})->name('about');

Route::post('/departments', "CourseController@getDepartments")->name('home.departments');

Route::get('/kursi', function () {
    return view('front.course-intro');
})->name("intro");

Route::get('/login', function () {
    \App\Http\Classes\Utils::unsetLogin();
    return view('front.login_front');
})->name("login");

Route::get('/list', "CourseController@index")->name("course_list");

Route::get('/search{p?}{f?}{d?}', "CourseController@search")->name("course_search");

Route::post('/login_verify', 'LoginController@login')->name("login_confirm");
Route::get('/kursi/{id}', "CourseController@courseIntro")->name("course_intro");
Route::post('/kursi/komente', "CourseController@comments")->name("course_comments");
Route::post('/kursi/komente/save', "CourseController@addComment")->name("save_comment");

Route::post('/kursi/reviews', "CourseController@reviews")->name("course_review");
Route::post('/kursi/reviews/save', "CourseController@addReview")->name("save_review");

Route::group(['middleware' => 'isLogged'], function () {

    Route::get('/profile', function () {
        return view('front.profile');
    })->name("profile");

    Route::get('/storage/{path}', 'CourseController@download')->name("download");

    //Routet vete te studenteve
    Route::group(['middleware' => 'isStudent'], function () {

        Route::get('/kurset-e-mia', "StudentController@index")->name("student_courses");
        Route::get('/kurset', "StudentController@listAllCourses")->name("stud_list_courses");
        Route::post('/ndiq_kursin', "StudentController@ndiqKursin")->name("ndiq_kursin");

    });

    //Routet vetem te lektoreve
    Route::group(['middleware' => 'isLecturer'], function () {

        Route::get('/lendet-e-mia', "LecturerController@index")->name("lecturer_courses");
        Route::get('/lendet', "LecturerController@listAllCourses")->name("lect_list_courses");
        Route::get('/edit/{id}', "CourseController@editCourse")->name("edit_course");


        Route::post('/edit/upload', "CourseController@uploadResource")->name("course.upload");
        Route::post('/edit/save', "CourseController@saveCourse")->name("course.save");
        Route::post('/edit/delete', "CourseController@deleteCourse")->name("course.delete");

    });
});
