<?php

namespace App\Models;

use App\Http\Classes\StudentClass;
use Illuminate\Database\Eloquent\Model;

class StudentModel extends Model
{
    protected $table = StudentClass::TABLE_NAME;
    protected $primaryKey = StudentClass::ID;
    public $timestamps = true;
}
