<?php

use App\Http\Classes\LoginClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LoginClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(LoginClass::ID);
            $table->string(LoginClass::UNAME)->unique();
            $table->string(LoginClass::UPASS);
            $table->string(LoginClass::ROLE);
            $table->smallInteger(LoginClass::STATUS);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
