<?php

use App\Http\Classes\CourseClass;
use App\Http\Classes\LectureCourseClass;
use App\Http\Classes\LecturerClass;
use App\Http\Classes\LoginClass;
use App\Models\CourseModel;
use App\Models\DepartmentModel;
use App\Models\LectureCourseModel;
use App\Models\LecturerModel;
use App\Models\LoginModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UniversitySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //krijohet nje fakultet
        $f1 = new \App\Models\FacultyModel();
        $f1->name = "Fakulteti Teknologjise se Informacionit";
        $f1->status = \App\Http\Classes\FacultyClass::STATUS_ACTIVE;
        $f1->save();

        //krijohet nje Prof ne tbl_login
        $head_log1= new LoginModel();
        $head_log1->uname = "sbushati";
        $head_log1->upass = Hash::make("123456");
        $head_log1->role = LoginClass::LECTURER;
        $head_log1->status = LoginClass::STATUS_ACTIVE;
        $head_log1->save();

        //region krijohet DEP1
        $dep1 = new DepartmentModel();
        $dep1->id_faculty = $f1->id;
//        $dep1->id_dephead = $log1->id;
        $dep1->name = "Shkenca Kompjuterike";
        $dep1->status = \App\Http\Classes\DepartmentClass::STATUS_ACTIVE;
        $dep1->save();

        //krijohet nje Lektor ( shefi departamentit)
        $head1 = new LecturerModel();
        $head1->name = "Senada";
        $head1->surname = "Bushati";
        $head1->title = "Dr.";
        $head1->id_login = $head_log1->id;
        $head1->id_department = $dep1->id;
        $head1->dep_head = LecturerClass::DEPHEAD_TRUE;
        $head1->status = LecturerClass::STATUS_ACTIVE;
        $head1->save();

        //region KRIJOHET LENDA 1

        $lend1 = new CourseModel();
        $lend1->id_department =  $dep1->id;
        $lend1->title =  "Programim ne C++";
        $lend1->desc =  "Programim ne C++";
        $lend1->viti =  1;
        $lend1->duration =  "14 jave";
        $lend1->status =  CourseClass::STATUS_ACTIVE;
        $lend1->save();

        //krijohet profesori ne Login
        $log1_1= new LoginModel();
        $log1_1->uname = "kbozhiqi";
        $log1_1->upass = Hash::make("123456");
        $log1_1->role = LoginClass::LECTURER;
        $log1_1->status = LoginClass::STATUS_ACTIVE;
        $log1_1->save();

        //krijohet profesori ne LecturerTable
        $lect1_1= new LecturerModel();
        $lect1_1->name = "Kristel";
        $lect1_1->surname = "Bozhiqi";
        $lect1_1->title = "Msc.";
        $lect1_1->id_login = $log1_1->id;
        $lect1_1->id_department = $dep1->id;
        $lect1_1->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect1_1->status = LecturerClass::STATUS_ACTIVE;
        $lect1_1->save();

        //krijohet connection i Prof me Lenden perkatese
        $lect_course1_1 = new LectureCourseModel();
        $lect_course1_1->id_course = $lend1->id;
        $lect_course1_1->id_lecture = $lect1_1->id;
        $lect_course1_1->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course1_1->save();

        //krijohet profesori tjtr ne Login
        $log1_2= new LoginModel();
        $log1_2->uname = "agjecka";
        $log1_2->upass = Hash::make("123456");
        $log1_2->role = LoginClass::LECTURER;
        $log1_2->status = LoginClass::STATUS_ACTIVE;
        $log1_2->save();

        //krijohet profesori ne LecturerTable
        $lect1_2= new LecturerModel();
        $lect1_2->name = "Anxhela";
        $lect1_2->surname = "Gjecka";
        $lect1_2->title = "Msc.";
        $lect1_2->id_login = $log1_2->id;
        $lect1_2->id_department = $dep1->id;
        $lect1_2->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect1_2->status = LecturerClass::STATUS_ACTIVE;
        $lect1_2->save();

        //krijohet connection i Prof me Lenden perkatese
        $lect_course1_2 = new LectureCourseModel();
        $lect_course1_2->id_course = $lend1->id;
        $lect_course1_2->id_lecture = $lect1_2->id;
        $lect_course1_2->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course1_2->save();

        //endregion

        //region KRIJOHET LENDA 2
        $lend2 = new CourseModel();
        $lend2->id_department =  $dep1->id;
        $lend2->title =  "Hyrje ne Programim";
        $lend2->desc =  "Hyrje ne Programim";
        $lend2->viti =  1;
        $lend2->duration =  "14 jave";

        $lend2->status =  CourseClass::STATUS_ACTIVE;
        $lend2->save();


        //krijohet profesori ne Login
        $log2_1= new LoginModel();
        $log2_1->uname = "vshtino";
        $log2_1->upass = Hash::make("123456");
        $log2_1->role = LoginClass::LECTURER;
        $log2_1->status = LoginClass::STATUS_ACTIVE;
        $log2_1->save();

        //krijohet profesori ne LecturerTable
        $lect2_1= new LecturerModel();
        $lect2_1->name = "Viola";
        $lect2_1->surname = "Shtino";
        $lect2_1->title = "Msc.";
        $lect2_1->id_login = $log2_1->id;
        $lect2_1->id_department = $dep1->id;
        $lect2_1->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect2_1->status = LecturerClass::STATUS_ACTIVE;
        $lect2_1->save();

        $lect_course2_1 = new LectureCourseModel();
        $lect_course2_1->id_course = $lend2->id;
        $lect_course2_1->id_lecture = $lect2_1->id;
        $lect_course2_1->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course2_1->save();

        //krijohet profesori ne Login
        $log2_2= new LoginModel();
        $log2_2->uname = "rkapuciu";
        $log2_2->upass = Hash::make("123456");
        $log2_2->role = LoginClass::LECTURER;
        $log2_2->status = LoginClass::STATUS_ACTIVE;
        $log2_2->save();

        //krijohet profesori ne LecturerTable
        $lect2_2= new LecturerModel();
        $lect2_2->name = "Rinela";
        $lect2_2->surname = "Kapçiu";
        $lect2_2->title = "Dr.";
        $lect2_2->id_login = $log2_2->id;
        $lect2_2->id_department = $dep1->id;
        $lect2_2->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect2_2->status = LecturerClass::STATUS_ACTIVE;
        $lect2_2->save();

        $lect_course2_2 = new LectureCourseModel();
        $lect_course2_2->id_course = $lend2->id;
        $lect_course2_2->id_lecture = $lect2_2->id;
        $lect_course2_2->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course2_2->save();
        //endregion

        //endregion


        //krijohet nje Prof ne tbl_login
        $head_log2= new LoginModel();
        $head_log2->uname = "ltoti";
        $head_log2->upass = Hash::make("123456");
        $head_log2->role = LoginClass::LECTURER;
        $head_log2->status = LoginClass::STATUS_ACTIVE;
        $head_log2->save();

        //region krijohet DEP2
        $dep2 = new DepartmentModel();
        $dep2->id_faculty = $f1->id;
        $dep2->name = "Teknologji Informacioni";
        $dep2->status = \App\Http\Classes\DepartmentClass::STATUS_ACTIVE;
        $dep2->save();

        //krijohet nje Lektor ( shefi departamentit)
        $head2 = new LecturerModel();
        $head2->name = "Luciana";
        $head2->surname = "Toti";
        $head2->title = "Prof.";
        $head2->id_login = $head_log2->id;
        $head2->id_department = $dep2->id;
        $head2->dep_head = LecturerClass::DEPHEAD_TRUE;
        $head2->status = LecturerClass::STATUS_ACTIVE;
        $head2->save();

        //endregion

        //region KRIJOHET LENDA 2
        $lend2_2 = new CourseModel();
        $lend2_2->id_department =  $dep2->id;
        $lend2_2->title =  "Magazinimi i te dhenave";
        $lend2_2->desc =  "Magazinimi i te dhenave";
        $lend2_2->viti =  1;
        $lend2_2->duration =  "14 jave";
        $lend2_2->status =  CourseClass::STATUS_ACTIVE;
        $lend2_2->save();

        //krijohet profesori ne Login
        $log2_21= new LoginModel();
        $log2_21->uname = "ezeqo";
        $log2_21->upass = Hash::make("123456");
        $log2_21->role = LoginClass::LECTURER;
        $log2_21->status = LoginClass::STATUS_ACTIVE;
        $log2_21->save();

        //krijohet profesori ne LecturerTable
        $lect2_21= new LecturerModel();
        $lect2_21->name = "Eris";
        $lect2_21->surname = "Zeqo";
        $lect2_21->title = "Prof.";
        $lect2_21->id_login = $log2_21->id;
        $lect2_21->id_department = $dep2->id;
        $lect2_21->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect2_21->status = LecturerClass::STATUS_ACTIVE;
        $lect2_21->save();

        $lect_course2_21 = new LectureCourseModel();
        $lect_course2_21->id_course = $lend2_2->id;
        $lect_course2_21->id_lecture = $lect2_21->id;
        $lect_course2_21->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course2_21->save();


        //krijohet profesori ne Login
        $log2_22= new LoginModel();
        $log2_22->uname = "egjergji";
        $log2_22->upass = Hash::make("123456");
        $log2_22->role = LoginClass::LECTURER;
        $log2_22->status = LoginClass::STATUS_ACTIVE;
        $log2_22->save();

        //krijohet profesori ne LecturerTable
        $lect2_22= new LecturerModel();
        $lect2_22->name = "Emiljana";
        $lect2_22->surname = "Gjergji";
        $lect2_22->title = "Prof.";
        $lect2_22->id_login = $log2_22->id;
        $lect2_22->id_department = $dep2->id;
        $lect2_22->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect2_22->status = LecturerClass::STATUS_ACTIVE;
        $lect2_22->save();

        $lect_course2_22 = new LectureCourseModel();
        $lect_course2_22->id_course = $lend2_2->id;
        $lect_course2_22->id_lecture = $lect2_22->id;
        $lect_course2_22->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course2_22->save();
        //endregion

        //endregion

        //region DEP3
        $dep3 = new DepartmentModel();
        $dep3->id_faculty = $f1->id;
        $dep3->name = "Matematike";
        $dep3->status = \App\Http\Classes\DepartmentClass::STATUS_ACTIVE;
        $dep3->save();

        //region KRIJOHET LENDA 1
        $lend3_1 = new CourseModel();
        $lend3_1->id_department =  $dep3->id;
        $lend3_1->title =  "Matematike e Zbatuar";
        $lend3_1->desc =  "Matematike e Zbatuar";
        $lend3_1->viti =  1;
        $lend3_1->duration =  "14 jave";

        $lend3_1->status =  CourseClass::STATUS_ACTIVE;
        $lend3_1->save();

        //krijohet profesori ne Login
        $log3_11= new LoginModel();
        $log3_11->uname = "tbiti";
        $log3_11->upass = Hash::make("123456");
        $log3_11->role = LoginClass::LECTURER;
        $log3_11->status = LoginClass::STATUS_ACTIVE;
        $log3_11->save();

        //krijohet profesori ne LecturerTable
        $lect3_11= new LecturerModel();
        $lect3_11->name = "Teuta";
        $lect3_11->surname = "Biti";
        $lect3_11->title = "Prof.";
        $lect3_11->id_login = $log3_11->id;
        $lect3_11->id_department = $dep3->id;
        $lect3_11->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect3_11->status = LecturerClass::STATUS_ACTIVE;
        $lect3_11->save();

        //krijohet connection i Prof me Lenden perkatese
        $lect_course3_11 = new LectureCourseModel();
        $lect_course3_11->id_course = $lend3_1->id;
        $lect_course3_11->id_lecture = $lect3_11->id;
        $lect_course3_11->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course3_11->save();



        //krijohet profesori tjtr ne Login
        $log3_12= new LoginModel();
        $log3_12->uname = "sliftaj";
        $log3_12->upass = Hash::make("123456");
        $log3_12->role = LoginClass::LECTURER;
        $log3_12->status = LoginClass::STATUS_ACTIVE;
        $log3_12->save();

        //krijohet profesori ne LecturerTable
        $lect3_12= new LecturerModel();
        $lect3_12->name = "Silvana";
        $lect3_12->surname = "Liftaj";
        $lect3_12->title = "Prof.";
        $lect3_12->id_login = $log3_12->id;
        $lect3_12->id_department = $dep3->id;
        $lect3_12->dep_head = LecturerClass::DEPHEAD_FALSE;
        $lect3_12->status = LecturerClass::STATUS_ACTIVE;
        $lect3_12->save();

        //krijohet connection i Prof me Lenden perkatese
        $lect_course3_12 = new LectureCourseModel();
        $lect_course3_12->id_course = $lend3_1->id;
        $lect_course3_12->id_lecture = $lect3_12->id;
        $lect_course3_12->status = LectureCourseClass::STATUS_ACTIVE;
        $lect_course3_12->save();
        //endregion

        //endregion

        //krijohet nje fakultet
        $f2 = new \App\Models\FacultyModel();
        $f2->name = "Fakulteti i Biznesit";
        $f2->status = \App\Http\Classes\FacultyClass::STATUS_ACTIVE;
        $f2->save();

        //region DEP1
        $dep2_1 = new DepartmentModel();
        $dep2_1->id_faculty = $f2->id;
        $dep2_1->name = "Shkenca Ekonomike";
        $dep2_1->status = \App\Http\Classes\DepartmentClass::STATUS_ACTIVE;
        $dep2_1->save();

        //krijohet nje Prof ne tbl_login
//        $head_log2= new LoginModel();
//        $head_log2->uname = "mibro";
//        $head_log2->upass = Hash::make("123456");
//        $head_log2->role = LoginClass::LECTURER;
//        $head_log2->status = LoginClass::STATUS_ACTIVE;
//        $head_log2->save();

        //krijohet nje Lektor ( shefi departamentit)
//        $head4 = new LecturerModel();
//        $head4->name = "Marsida";
//        $head4->surname = "Ibro";
//        $head4->title = "Msc.";
//        $head4->id_login = $head_log2->id;
//        $head4->id_department = $dep2->id;
//        $head4->dep_head = LecturerClass::DEPHEAD_FALSE;
//        $head4->status = LecturerClass::STATUS_ACTIVE;
//        $head4->save();
//
//        $lend3_1 = new CourseModel();
//        $lend3_1->id_department =  $dep2->id;
//        $lend3_1->title =  "Dizenjim i Logjikes Dixhitale";
//        $lend3_1->desc =  "Dizenjim i Logjikes Dixhitale";
//        $lend3_1->viti =  1;
//        $lend3_1->duration =  "14 jave";
//        $lend3_1->status =  CourseClass::STATUS_ACTIVE;
//        $lend3_1->save();
//
//        $lect_course_ld1 = new LectureCourseModel();
//        $lect_course_ld1->id_course = $lend3_1->id;
//        $lect_course_ld1->id_lecture = $head_log2->id;
//        $lect_course_ld1->status = LectureCourseClass::STATUS_ACTIVE;
//        $lect_course_ld1->save();
//
//        $lect_course_ld2 = new LectureCourseModel();
//        $lect_course_ld2->id_course = $lend3_1->id;
//        $lect_course_ld2->id_lecture = $head4->id;
//        $lect_course_ld2->status = LectureCourseClass::STATUS_ACTIVE;
//        $lect_course_ld2->save();

    }
}
