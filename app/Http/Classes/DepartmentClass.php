<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 4:14 PM
 */

namespace App\Http\Classes;


class DepartmentClass
{
    const TABLE_NAME = "department";
    const ID = "id";
    const NAME = "name";
    const ID_FACULTY = "id_faculty";
//    const ID_DEPHEAD= "id_dephead";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}