@extends("layouts.prime")
@section("title")
    UAMD-learn
@endsection

@section("styles")
@endsection

@section("content")
    <body id="page-top" class="home">

    <div id="page-wrap">

        <!-- PRELOADER -->
        <div id="preloader">
            <div class="pre-icon">
                <div class="pre-item pre-item-1"></div>
                <div class="pre-item pre-item-2"></div>
                <div class="pre-item pre-item-3"></div>
                <div class="pre-item pre-item-4"></div>
            </div>
        </div>
        <!-- END / PRELOADER -->

        @include("layouts.headerMenu")

        <!-- HOME SLIDER -->
        <section class="slide" style="background-image: url(images/homeslider/bg.jpg)">
            <div class="container">
                <div class="slide-cn" id="slide-home">
                    <!-- SLIDE ITEM -->
                    <div class="slide-item">
                        <div class="item-inner">
                            <div class="text">
                                <h2>E-Learning</h2>
                                <p>Sistemi menaxhues i kurseve<br> i shpejte dhe i lehte
                                </p>
                            </div>

                            <div class="img">
                                <img src="images/homeslider/img-thumb.png" alt="">
                            </div>
                        </div>

                    </div>
                    <!-- SLIDE ITEM -->

                    <!-- SLIDE ITEM -->
                    <div class="slide-item">
                        <div class="item-inner">
                            <div class="text">
                                <h2>E-Learning</h2>
                                <p>Sistemi menaxhues i kurseve<br>me akses per cdo student<br>dhe pedagog
                                </p>
                            </div>

                                 <div class="img">
                                     <img src="images/homeslider/img-thumb.png" alt="">
                                 </div>

                             </div>
                         </div>
                         <!-- SLIDE ITEM -->

                </div>
            </div>
        </section>
        <!-- END / HOME SLIDER -->

        <!-- AFTER SLIDER -->
        <section id="after-slider" class="after-slider section">
            <div class="awe-color bg-color-1"></div>
            <div class="after-slider-bg-2"></div>
            <div class="container" style="width: 1260px;">
                {{ Form::open(array('id'=>'searchForm','method'=>'get','route' => 'course_search', 'class' => 'form')) }}
                <div class="after-slider-content tb">

                    <div class="inner tb-cell" style="width:85%; padding-right: 12px">

                        <h4 style="width: 14%">Gjej kursin tënd</h4>
                        <div class="course-keyword" style="width: 30%">
                            <input type="text" id="search_course" name="q" placeholder="Emri">
                        </div>
                        <div class="mc-select-wrap" style="width: 30%">
                            <div class="mc-select">
                                <select class="select" name="f" id="faculty">
                                    <option value="" selected>Fakulteti</option>
                                    @foreach($faculty as $f)
                                        <option value="{!! $f->id !!}">{!!$f->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="mc-select-wrap"  style="width: 24%">
                            <div class="mc-select dep_parent">
                                <select class="select" name="d" id="dep">
                                    <option value="" selected>Departamenti</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="tb-cell text-right">
                        <div class="form-actions">
                            <input type="button" id="kerko" value="Gjej Kursin" class="mc-btn btn-style-1">
                        </div>
                    </div>

                </div>
                {{ Form::close() }}

            </div>

        </section>
        <!-- END / AFTER SLIDER -->

        <!-- SECTION 1 -->
        <section id="mc-section-1" class="mc-section-1 section">
            <div class="container">
                <div class="row">

                    <div class="col-md-5">
                        <div class="mc-section-1-content-1">
                            <h2 class="big">Menaxhimi i kurseve online</h2>
                            <p class="mc-text">Kjo platforme E-Learning mundeson lehtesim ne bashkeveprimin student-pedagog. Ajo jep mundesi si padagogut edhe studenti te logohen . Pedagogu e perdor ate per te ngarkuar materialet(lesione, ushtrime) te kursit qe jep per stuentet e tij. Nga ana tjeter studentit i lejohet te aksesoj matariale te pedagogve te fakultetit te tij. </p>
                            <a href="{!! \Illuminate\Support\Facades\URL::route('about') !!}" class="mc-btn btn-style-1">Rreth nesh</a>
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-offset-1">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="featured-item">
                                    <i class="icon icon-featured-1"></i>
                                    <h4 class="title-box text-uppercase">SIGURIA</h4>
                                    <p>Siguria e te dhenave tuaja. Lejimi i redaktimit te tyre vetem nga personat e
                                        autorizuar.</p>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="featured-item">
                                    <i class="icon icon-featured-2"></i>
                                    <h4 class="title-box text-uppercase">Lehtesi ne te mesuar</h4>
                                    <p>Marrja leksioneve ne kohe reale dhe  perditesimet e fundit.</p>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="featured-item">
                                    <i class="icon icon-featured-3"></i>
                                    <h4 class="title-box text-uppercase">SUPORTI</h4>
                                    <p>Mbeshtetja dhe bashkeveprimi me pedagogun ne cdo kohe. </p>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="featured-item">
                                    <i class="icon icon-featured-4"></i>
                                    <h4 class="title-box text-uppercase">PERFORMANCA</h4>
                                    <p>Performanca e larte ne aksesimin e te dhenave.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- END / SECTION 1 -->

        <!-- SECTION 2 -->
        <section id="mc-section-2" class="mc-section-2 section">
            <div class="awe-parallax bg-section1-demo"></div>
            <div class="overlay-color-1"></div>
            <div class="container">
                <div class="section-2-content">
                    <div class="row">

                        <div class="col-md-5">
                            <div class="ct">
                                <h2 class="big">Mesimi me i lehte se kurre </h2>
                                <p class="mc-text">Kontribut te plote ne bashkeveprimin student-pedagog.<br>
                                    Akses te shpejte nga cdo lloj pajisje.<br>Nje zgjidhje per cdo pyetje.<br>
                                     Marrje e nje feedback-u te menjehershem.</p>
                                <a href="{!! route('course_list') !!}" class="mc-btn btn-style-3">Kurset</a>
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="image">
                                <img src="images/image.png" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- END / SECTION 2 -->

        <!-- SECTION 3 -->
        <section id="mc-section-3" class="mc-section-3 section">
            <div class="container">
                <!-- FEATURE -->
                <div class="feature-course">
                    <h4 class="title-box text-uppercase">KURSET E VEÇUARA</h4>
                    <a href="{!! route('course_list') !!}" class="all-course mc-btn btn-style-1">Shiko te gjitha</a>
                    <div class="row">
                        <div class="feature-slider">
                            @foreach($courses as $course )
                            <div class="mc-item mc-item-1">
                                <div class="image-heading">
                                    <img src="{!! $course->image !!}" alt="">
                                </div>
                                <div class="meta-categories"><a href="#">Programim ne C++</a></div>
                                <div class="content-item">
                                    <div class="image-author">
                                        <img src="images/avatar-1.jpg" alt="">
                                    </div>
                                    <h4><a href="{!! \Illuminate\Support\Facades\URL::route("course_intro",
                                                        $course->lecture_course_id) !!}">
                                            {!! $course->course_name !!}</a></h4>
                                    <div class="name-author">
                                         <a href="javascript:void(0)">
                                            {!! $course->title !!} {!! $course->name !!} {!! $course->surname !!}
                                        </a>
                                    </div>
                                </div>
                                <div class="ft-item">
                                    <div class="rating">
                                        <div class="rating">
                                            @for($j = 0; $j < $course->review; $j++)
                                                <a href="#" class="active"></a>
                                            @endfor

                                            @if($course->review < 5)
                                                @for($i = 0; $i < 5-$course->review; $i++)
                                                    <a href="#"></a>
                                                @endfor
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- END / FEATURE -->
            </div>
        </section>
        <!-- END / SECTION 3 -->

        @include("layouts.footerMenu")

    </div>
        @include("layouts.footerScripts")
    <script>
        $(function () {

            $("#faculty").on("change", function () {
                var name = $(this).val();
                $.ajax({
                        url: '{!! route('home.departments') !!}',
                        type: 'POST',

                        data:{
                            _token: "{{ csrf_token() }}",
                            q: name
                        },
                        success:function (data) {
//                            console.log(data.dep);
//                            console.log(dep.length);
                            if (data.status === 1){
                                var dep = $.parseJSON(data.dep);
                                var html = '<option value="" selected>Departamenti</option>';
                                for(i =0; i < dep.length; i++){
                                    html += '<option value="'+dep[i].id+'">'+dep[i].name+'</option>';
                                }
                                $("#dep").select().html(html);
                                $(".dep_parent").find('span.select').html("Departamenti");
//                                $("#dep").html(html);
                            }
                        }
                    }
                )
            });

            $("#kerko").on('click', function (e) {

                    $("#searchForm").submit();
            });
        });
    </script>
    </body>
@endsection


