<?php

namespace App\Http\Controllers;

use App\Http\Classes\CourseClass;
use App\Http\Classes\DepartmentClass;
use App\Http\Classes\FacultyClass;
use App\Http\Classes\LectureCourseClass;
use App\Http\Classes\LecturerClass;
use App\Http\Classes\LessonClass;
use App\Http\Classes\LessonResourceClass;
use App\Http\Classes\LoginClass;
use App\Http\Classes\ReviewClass;
use App\Http\Classes\StudentClass;
use App\Http\Classes\StudentCourseClass;
use App\Http\Classes\UserCommentClass;
use App\Http\Classes\Utils;
use App\Models\DepartmentModel;
use App\Models\FacultyModel;
use App\Models\LectureCourseModel;
use App\Models\LecturerModel;
use App\Models\LessonModel;
use App\Models\LessonResourceModel;
use App\Models\LoginModel;
use App\models\ReviewModel;
use App\Models\StudentCourseModel;
use App\Models\StudentModel;
use App\models\UserCommentsModel;
use Exception;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Integer;
use PhpParser\Node\Expr\Cast\Object_;

class CourseController extends Controller
{
    public function homeView()
    {

        $facluty = FacultyModel::get();

        $courses = DB::table(CourseClass::TABLE_NAME)
            ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name", CourseClass::TABLE_NAME . '.' . CourseClass::ID,
                LectureCourseClass::TABLE_NAME.'.'.LectureCourseClass::ID.' as lecture_course_id',
                CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::DESC,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)
            ->join(DepartmentClass::TABLE_NAME, DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                CourseClass::ID_DEPARTMENT)
            ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE,
                CourseClass::TABLE_NAME . '.' . CourseClass::ID)
            ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
            ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE)
            ->get();

        $listCourse = [];
        foreach ($courses as $course){
            $totRev = ReviewModel::where(ReviewClass::ID_LECT_COURSE, $course->lecture_course_id)
                ->avg(ReviewClass::REVIEW);

            $obj=(object)[
                'course_name'=> $course->course_name,
                'id'=>$course->id,
                'lecture_course_id'=>$course->lecture_course_id,
                'image'=>$course->image,
                'desc'=>$course->desc,
                'title'=>$course->title,
                'name'=>$course->name,
                'surname'=>$course->surname,
                'review' => round($totRev),
            ];
            array_push($listCourse, $obj);
        }

        return view('front.home')
            ->with("courses", $listCourse)
            ->with("faculty", $facluty)
            ;
    }

    public function getDepartments(Request $request)
    {
        if (isset($request->q)) {
            $faculty = htmlentities(trim($request->q));

            $department = DepartmentModel::select(DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                DepartmentClass::TABLE_NAME . '.' . DepartmentClass::NAME)
                ->where(DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID_FACULTY, $faculty)
                ->where(DepartmentClass::TABLE_NAME . '.' . DepartmentClass::STATUS, DepartmentClass::STATUS_ACTIVE)
                ->get();

            return [
                'status' => 1,
                'dep' => json_encode($department),
            ];
        }

    }

    public function index()
    {
        if (Utils::isLoggedIn()) {

            //merr gjithe kurset per departamentin perkates
            if (Utils::isStudent()) {

                return Redirect::route("stud_list_courses");

            } else if (Utils::isLecturer()) {

                return Redirect::route("lect_list_courses");

            } else {
                return Redirect::back();
            }
        } else {
            $courses = DB::table(CourseClass::TABLE_NAME)
                ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name",
                    LectureCourseClass::TABLE_NAME . '.' . CourseClass::ID,
                    LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID . ' as lecture_course_id',
                    CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::DESC,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)

                ->join(DepartmentClass::TABLE_NAME, DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                    CourseClass::ID_DEPARTMENT)
                ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE,
                    CourseClass::TABLE_NAME . '.' . CourseClass::ID)
                ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                    LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
                ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE)
                ->get();

            $listCourse = [];
            foreach ($courses as $course){
                $totRev = ReviewModel::where(ReviewClass::ID_LECT_COURSE, $course->lecture_course_id)
                    ->avg(ReviewClass::REVIEW);

                $obj=(object)[
                    'course_name'=> $course->course_name,
                    'id'=>$course->id,
                    'lecture_course_id'=>$course->lecture_course_id,
                    'image'=>$course->image,
                    'desc'=>$course->desc,
                    'title'=>$course->title,
                    'name'=>$course->name,
                    'surname'=>$course->surname,
                    'review' => round($totRev),
                ];
                array_push($listCourse, $obj);
            }

            //merr gjithe kurset e mundshme
            return view("front.course_list")
                ->with("courses", $listCourse);
        }
    }

    public function search(Request $request)
    {
        $param1 = trim(htmlentities($request->q));

        $courses = DB::table(CourseClass::TABLE_NAME)
            ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name", CourseClass::TABLE_NAME . '.' . CourseClass::ID,
                LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID . ' as lecture_course_id',
                CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::DESC,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)
            ->join(DepartmentClass::TABLE_NAME, DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                CourseClass::ID_DEPARTMENT)
            ->join(StudentClass::TABLE_NAME, StudentClass::TABLE_NAME . '.' . StudentClass::ID_DEPARTMENT,
                DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID)
            ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE,
                CourseClass::TABLE_NAME . '.' . CourseClass::ID)
            ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
            ->where(function ($query) use ($param1) {
                $query->where(CourseClass::TABLE_NAME . '.' . CourseClass::NAME, 'like', "%" . $param1 . "%")
                    ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE);
            })
            ->orWhere(function ($query) use ($param1) {
                $query->where(CourseClass::TABLE_NAME . '.' . CourseClass::DESC, 'like', "%" . $param1 . "%")
                    ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE);
            })
            ->get();


        if (count($courses) > 0) {

            $listCourse = [];
            foreach ($courses as $course){
                $totRev = ReviewModel::where(ReviewClass::ID_LECT_COURSE, $course->lecture_course_id)
                    ->avg(ReviewClass::REVIEW);

                $obj=(object)[
                    'course_name'=> $course->course_name,
                    'id'=>$course->id,
                    'lecture_course_id'=>$course->lecture_course_id,
                    'image'=>$course->image,
                    'desc'=>$course->desc,
                    'title'=>$course->title,
                    'name'=>$course->name,
                    'surname'=>$course->surname,
                    'review' => round($totRev),
                ];
                array_push($listCourse, $obj);
            }

            return view("front.search_result")
                ->with("param", $param1)
                ->with("dep", [])
                ->with("courses", $listCourse);
        }

        $departamente = DepartmentModel::select(DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
            DepartmentClass::TABLE_NAME . '.' . DepartmentClass::NAME . ' as dname',
            FacultyClass::TABLE_NAME . '.' . FacultyClass::NAME)
            ->join(FacultyClass::TABLE_NAME, FacultyClass::TABLE_NAME . '.' . FacultyClass::ID, DepartmentClass::ID_FACULTY)
            ->where(DepartmentClass::TABLE_NAME . '.' . DepartmentClass::STATUS, DepartmentClass::STATUS_ACTIVE)
            ->get();


        return view("front.search_notfound")
            ->with("param", isset($param1) ? $param1 : "")
            ->with("courses", [])
            ->with("dep", $departamente);


    }

    public function courseIntro($id)
    {
        try {

            if (isset($id)) {
                $id = trim(htmlentities($id));
                $lect_course = LectureCourseModel::select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . ' as course_name',
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, CourseClass::TABLE_NAME . '.' . CourseClass::START,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME,
                    CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::VITI,
                    CourseClass::TABLE_NAME . '.' . CourseClass::DURATION, CourseClass::DESC,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                    LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID . ' as lect_course_id')
                    ->join(CourseClass::TABLE_NAME, CourseClass::TABLE_NAME . '.' . CourseClass::ID,
                        LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE)
                    ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                        LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
                    ->where(LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID, $id)
                    ->first();

                if ($lect_course) {
                    session(["lect_course_id" => $id]);

                    if (!Utils::isLoggedIn()) {

                        return Redirect::route("login");
                    }

                    Session::forget("lect_course_id");

                    $leksionet = array();
                    $lessons = LessonModel::where(LessonClass::TABLE_NAME . '.' . LessonClass::ID_LECTURE_COURSE, $id)
                        ->where(LessonClass::TABLE_NAME . '.' . LessonClass::STATUS, LessonClass::STATUS_ACTIVE)
                        ->orderBy(LessonClass::ID, 'asc')
                        ->get();

                    $nr_lexione = 0;
                    for ($i = 0; $i < count($lessons); $i++) {
                        array_push($leksionet, array("id" => $lessons[$i]->id, "title" => $lessons[$i]->title, "item" => []));

                        $res = LessonResourceModel::where(LessonResourceClass::TABLE_NAME . '.' . LessonResourceClass::ID_LECTURE_COURSE,
                            $lessons[$i]->id)
                            ->get();

                        $resources = [];
                        for ($j = 0; $j < count($res); $j++) {
                            array_push($leksionet[$i]["item"], (object)["title" => $res[$j]->title, "id" => $res[$j]->id, "path" => $res[$j]->path]);
                        }

                        $nr_lexione++;
                    }

                    $isMine = false;
                    $isAttending = false;
                    $nrStud = 0;
                    $students = [];

                    if (Utils::isLecturer()) {

                        if ($lect_course->id == Utils::getUserId()) {
                            $isMine = true;
                            $students = StudentCourseModel::select(StudentClass::TABLE_NAME . '.' . StudentClass::NAME,
                                StudentClass::TABLE_NAME . '.' . StudentClass::SURNAME)
                                ->join(StudentClass::TABLE_NAME, StudentClass::TABLE_NAME . '.' . StudentClass::ID,
                                    StudentCourseClass::ID_STUDENT)
                                ->where(StudentCourseClass::TABLE_NAME . '.' . StudentCourseClass::ID_LECTURE_COURSE, $id)
                                ->where(StudentCourseClass::TABLE_NAME . '.' . StudentCourseClass::STATUS, 1)
                                ->get();
                        }


                    } else if (Utils::isStudent()) {

                        $user = StudentModel::where(StudentClass::ID, Utils::getUserId())
                            ->first();

                        if ($lect_course->viti == $user->viti) {
                            $isMine = true;
                        }
                        if (Utils::isAttendingThis($id)) {
                            $isAttending = true;
                        }
                    }

                    return view("front.course-intro")
                        ->with("course", $lect_course)
                        ->with("isMine", $isMine)
                        ->with("nr_lexione", $nr_lexione)
                        ->with("students", $students)
                        ->with("isAttending", $isAttending)
                        ->with("lexion", (object)$leksionet);
                } else {
                    abort(404);
                }
            } else {
                abort(404);
            }
        } catch (Exception $e) {
            return Redirect::route("home");
        }
    }

    public function comments(Request $request)
    {
        if (isset($request->id)) {
            $id_lect_course = $request->id;
            $html = '';

            $coments = UserCommentsModel::select(UserCommentClass::TABLE_NAME.'.'.UserCommentClass::ID, UserCommentClass::COMMENT,
                LoginClass::ROLE, UserCommentClass::ID_LOGIN, UserCommentClass::TABLE_NAME.'.created_at')
                ->join(LoginClass::TABLE_NAME, LoginClass::TABLE_NAME . '.' . LoginClass::ID,
                    UserCommentClass::ID_LOGIN)
                ->where(UserCommentClass::TABLE_NAME . '.' . UserCommentClass::ID_LECT_COURSE, $id_lect_course)
                ->where(UserCommentClass::TABLE_NAME . '.' . UserCommentClass::STATUS, UserCommentClass::STATUS_ACTIVE)
                ->orderBy(UserCommentClass::TABLE_NAME.'.'.UserCommentClass::ID, 'desc')
                ->get();

            $commentnr = count($coments);

            foreach($coments as $coment){

                $user = '';
                if (isset($coment->role) && isset($coment->id_login)) {

                    $role = $coment->role;
                    $id_login = $coment->id_login;
                    $date = $coment->created_at != null ? $coment->created_at : time();
                    $content = $coment->comment != null ? $coment->comment : "";
                    $date = date('H:i, d M Y', strtotime($date));

                    if ($role == LoginClass::STUDENT) {
                        $user = StudentModel::where(StudentClass::TABLE_NAME . '.' . StudentClass::ID_LOGIN, $id_login)
                            ->first();

                        if ($user) {
                            $user = $user->name . ' ' . $user->surname;
                        }
                    }else if($role == LoginClass::LECTURER){
                        $user = LecturerModel::where(LecturerClass::TABLE_NAME . '.' . LecturerClass::ID_LOGIN,
                                                    $id_login)
                            ->first();

                        if ($user) {
                            $user = $user->title.' '.$user->name . ' ' . $user->surname;
                        }
                    }

                    $html .= '<li class="review">
                                            <div class="body-review">
                                                <div class="content-review">
                                                    <h4 class="sm black">
                                                        <a href="javascript:void(0)">' . $user . '</a>
                                                    </h4>

                                                    <em>' . $date . '</em>
                                                    <p><b>"</b> ' . $content . ' <b>"</b></p>
                                                </div>
                                            </div>
                                        </li>';

                }

            }

            return [
                'status' => 1,
                'commentnr' => $commentnr,
                'data' => $html,
            ];


        } else {
            return [
                'status' => 0
            ];
        }
    }

    public function addComment(Request $request){

        if(isset($request->id)){

            $id_lect_curse = $request->id;
            $content = $request->coment;

            $comment = new UserCommentsModel();
            $comment->id_login = Utils::getLoginId();
            $comment->id_lect_course = $id_lect_curse;
            $comment->comment = $content;
            $comment->status = UserCommentClass::STATUS_ACTIVE;
            $comment->save();

            $role =Utils::getRole();

            $user = '';
            if ($role == LoginClass::STUDENT) {
                $user = StudentModel::where(StudentClass::TABLE_NAME . '.' . StudentClass::ID_LOGIN, Utils::getLoginId())
                    ->first();

                if ($user) {
                    $user = $user->name . ' ' . $user->surname;
                }
            }else if($role == LoginClass::LECTURER){
                $user = LecturerModel::where(LecturerClass::TABLE_NAME . '.' . LecturerClass::ID_LOGIN,
                    Utils::getLoginId())
                    ->first();

                if ($user) {
                    $user = $user->title.' '.$user->name . ' ' . $user->surname;
                }
            }

            $html = '<li class="review">
                                            <div class="body-review">
                                                <div class="content-review">
                                                    <h4 class="sm black">
                                                        <a href="javascript:void(0)">' . $user . '</a>
                                                    </h4>

                                                    <em>' . date('H:i, d M Y', time()) . '</em>
                                                    <p><b>"</b> ' . $content . ' <b>"</b></p>
                                                </div>
                                            </div>
                                        </li>';

            return [
                'status' => 1,
                'html' => $html,
            ];
        }
        return [
            'status' => 0,
        ];

    }

    public function reviews(Request $request)
    {
        if (isset($request->id)) {
            $id_lect_course = $request->id;
            $html = '';

            $reviews = ReviewModel::select(ReviewClass::TABLE_NAME.'.'.ReviewClass::ID, ReviewClass::REVIEW,
                LoginClass::ROLE, ReviewClass::ID_LOGIN, ReviewClass::TABLE_NAME.'.created_at')
                ->join(LoginClass::TABLE_NAME, LoginClass::TABLE_NAME . '.' . LoginClass::ID,
                    ReviewClass::ID_LOGIN)
                ->where(ReviewClass::TABLE_NAME . '.' . ReviewClass::ID_LECT_COURSE, $id_lect_course)
                ->where(ReviewClass::TABLE_NAME . '.' . ReviewClass::STATUS, ReviewClass::STATUS_ACTIVE)
                ->orderBy(ReviewClass::TABLE_NAME.'.'.ReviewClass::ID, 'desc')
                ->get();

            $reviewsnr = count($reviews);
            $tot_rate = 0;

            foreach($reviews as $review){

                $user = '';
                if (isset($review->role) && isset($review->id_login)) {

                    $role = $review->role;
                    $id_login = $review->id_login;
                    $tot_rate +=$review->review;
                    $date = $review->created_at != null ? $review->created_at : time();
                    $review_value = $review->review != null ? $review->review : 0;
                    $date = date('H:i, d M Y', strtotime($date));

                    if ($role == LoginClass::STUDENT) {
                        $user = StudentModel::where(StudentClass::TABLE_NAME . '.' . StudentClass::ID_LOGIN, $id_login)
                            ->first();

                        if ($user) {
                            $user = $user->name . ' ' . $user->surname;
                        }
                    }

                    $html .= '<li class="review">
                                            <div class="body-review">
                                                <div class="content-review">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h4 class="sm black">
                                                                <a href="javascript:void(0)">' . $user . '</a>
                                                            </h4>
                                                            <em>' . $date . '</em>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="rating">';

                    for($i = 0; $i < $review_value; $i++){
                        $html .= '<a href="javascript:void(0)" class="active"></a>';
                    }
                    if($review_value < 5){
                        for ($j = 0; $j < 5-$review_value; $j++ ){
                            $html .= '<a href="javascript:void(0)" ></a>';
                        }
                    }

                    $html .= '</div></div></div></div></div>';
                } 
            }

            $tot_rate = round($tot_rate/$reviewsnr);

            $tot_ratehtml= '';
            for($i = 0; $i < $tot_rate; $i++){
                $tot_ratehtml .= '<a href="javascript:void(0)" class="active"></a>';
            }
            if($tot_rate < 5){
                for ($j = 0; $j < 5-$tot_rate; $j++ ){
                    $tot_ratehtml .= '<a href="javascript:void(0)" ></a>';
                }
            }
            return [
                'status' => 1,
                'reviewsnr' => $reviewsnr,
                'tot_rate_html' => $tot_ratehtml,
                'data' => $html,
            ];

        } else {
            return [
                'status' => 0
            ];
        }
    }

    public function addReview(Request $request){

        if(isset($request->id)){

            $id_lect_curse = $request->id;
            $content = $request->review;

            $comment = new ReviewModel();
            $comment->id_login = Utils::getLoginId();
            $comment->id_lect_course = $id_lect_curse;
            $comment->review = $content;
            $comment->status = ReviewClass::STATUS_ACTIVE;
            $comment->save();

            $user = StudentModel::where(StudentClass::TABLE_NAME . '.' . StudentClass::ID_LOGIN, Utils::getLoginId())
                ->first();

            if ($user) {
                $user = $user->name . ' ' . $user->surname;
            }
            $html = '<li class="review">
                                    <div class="body-review">
                                        <div class="content-review">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h4 class="sm black">
                                                        <a href="javascript:void(0)">' . $user . '</a>
                                                    </h4>
                                                    <em>' . date('H:i, d M Y', time()) . '</em>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="rating">';

            for($i = 0; $i < $content; $i++){
                $html .= '<a href="javascript:void(0)" class="active"></a>';
            }
            if($content < 5){
                for ($j = 0; $j < 5-$content; $j++ ){
                    $html .= '<a href="javascript:void(0)" ></a>';
                }
            }

            $html .= '</div></div></div></div></div></div>';

            return [
                'status' => 1,
                'html' => $html,
            ];
        }
        return [
            'status' => 0,
        ];

    }

    public function editCourse($lecture_course_id)
    {
        if (isset($lecture_course_id) && !empty($lecture_course_id)) {
            $lect_course_id = htmlentities($lecture_course_id);

            $leksionet = array();
            $lessons = LessonModel::where(LessonClass::TABLE_NAME . '.' . LessonClass::ID_LECTURE_COURSE, $lect_course_id)
                ->where(LessonClass::TABLE_NAME . '.' . LessonClass::STATUS, LessonClass::STATUS_ACTIVE)
                ->orderBy(LessonClass::ID, 'asc')
                ->get();

            $nr_lexione = 0;
            for ($i = 0; $i < count($lessons); $i++) {
                array_push($leksionet, array("id" => $lessons[$i]->id, "title" => $lessons[$i]->title, "item" => []));

                $res = LessonResourceModel::where(LessonResourceClass::TABLE_NAME . '.' . LessonResourceClass::ID_LECTURE_COURSE,
                    $lessons[$i]->id)
                    ->get();

                $resources = [];
                for ($j = 0; $j < count($res); $j++) {
                    array_push($leksionet[$i]["item"], (object)["title" => $res[$j]->title, "id" => $res[$j]->id, "path" => $res[$j]->path]);
                }

                $nr_lexione++;
            }

            $course = DB::table(CourseClass::TABLE_NAME)
                ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name",
                    LectureCourseClass::TABLE_NAME . '.' . CourseClass::ID,
                    LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID . ' as lecture_course_id',
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)
                ->join(DepartmentClass::TABLE_NAME, DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                    CourseClass::ID_DEPARTMENT)
                ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE,
                    CourseClass::TABLE_NAME . '.' . CourseClass::ID)
                ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                    LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
                ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE)
                ->where(LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID, $lect_course_id)
                ->first();

//            foreach ($leksionet as $l){
//                $l = (object)$l;
//                echo $l->id;
//            }
//            echo json_encode($leksionet);die();
            return view("front.create_unit")
                ->with("course", $course)
                ->with("nr_lexione", $nr_lexione)
                ->with("lexion", (object)$leksionet);
//            echo json_encode($lect_course);die();
        } else {
            abort(404);
        }

    }

    public function saveCourse(Request $request)
    {
        if (isset($request->id) && isset($request->title)) {

            try {
                DB::beginTransaction();
                $id_lect_course = $request->id;
                $lesson_title = $request->title;
                $lesson_id = $request->lesson_id;
                $string = "id: " . $id_lect_course . " title: " . $lesson_title . " leson_id: " . $lesson_id;
                if ($lesson_id != 0) {
                    //update lesson
                    $done = $lesson = LessonModel::where(LessonClass::TABLE_NAME . '.' . LessonClass::ID, $lesson_id)
                        ->update([
                            LessonClass::TITLE => $lesson_title,
                        ]);
                    $string .= " leson_updated";

                } else {

                    $newLesson = new LessonModel();
                    $newLesson->title = $lesson_title;
                    $newLesson->id_lect_course = $id_lect_course;
                    $newLesson->status = 1;
                    $newLesson->save();

                    $lesson_id = $newLesson->id;
                    $string .= " lesson_new_id: " . $lesson_id;

                }

                $string .= " -length-" . count($request->item_doc_path);
                for ($i = 0; $i < count($request->item_doc_path); $i++) {

                    $string .= " item_path [" . $i . "]: " . $request->item_doc_path[$i];

                    $exists = LessonResourceModel::where(LessonResourceClass::TABLE_NAME . '.' . LessonResourceClass::PATH,
                        $request->item_doc_path[$i])
                        ->first();

                    if (!$exists) {

                        $string .= " -> nuk egziston.";

                        $leson_res = new LessonResourceModel();
                        $leson_res->title = $request->item_doc_name[$i];
                        $leson_res->path = $request->item_doc_path[$i];
                        $leson_res->link = "";
                        $leson_res->id_lesson = $lesson_id;
                        $leson_res->type = LessonResourceClass::STATUS_ACTIVE;
                        $leson_res->status = LessonResourceClass::STATUS_ACTIVE;
                        $leson_res->save();

                        $string .= " item_resource [" . $i . "]: " . $leson_res->id;


                    } else {
                        $string .= " -> Update .";

                        LessonResourceModel::where(LessonResourceClass::TABLE_NAME . '.' . LessonResourceClass::PATH,
                            $request->item_doc_path[$i])
                            ->update([
                                LessonResourceClass::TITLE => $request->item_doc_name[$i]
                            ]);
                    }
                }
//                echo json_encode($request->item_doc_name);

                DB::commit();

                return [
                    'status' => 1,
                    'lesson_id' => $lesson_id,
                    'msg' => "Leksioni u ruajt me sukses! ",
                    'log' => $string
                ];
            } catch (Exception $e) {
                DB::rollBack();

                echo $e->getMessage() . ' [ ' . "error" . ' ]';
            }
        }
    }

    public function deleteCourse(Request $request)
    {
        if (isset($request->id)) {

            try {
                DB::beginTransaction();

                $lesson_id = $request->id;

                if ($lesson_id != 0) {
                    //fshin gjithe materialet e leksionit
                    $done_res = LessonResourceModel::where(LessonResourceClass::TABLE_NAME . '.' . LessonResourceClass::ID_LECTURE_COURSE,
                        $lesson_id)
                        ->delete();

                    //update lesson
                    $done = $lesson = LessonModel::where(LessonClass::TABLE_NAME . '.' . LessonClass::ID, $lesson_id)
                        ->delete();

                    if (!$done) {
                        DB::rollback();

                        return [
                            'sts' => 0,
                            'error' => "Ndodhi nje gabim dhe kursi nuk mund te fshihet!",
                        ];
                    }

                    DB::commit();

                    return [
                        'sts' => 1,
                        'msg' => ""
                    ];

                }

                DB::rollback();

                return [
                    'sts' => 0,
                    'msg' => ""
                ];
            } catch (Exception $e) {
                DB::rollBack();

                return [
                    'sts' => 0,
                    'error' => $e->getMessage()
                ];
            }

        } else {
            return [
                'sts' => 0,
                'msg' => ""
            ];
        }
    }

    public function uploadResource(Request $request)
    {
        if ($request->hasFile('document')) {
            $id = $request->id;

            $document = $request->file('document');
            $fileName = time() . '.' . $document->getClientOriginalExtension();

            $path = "assets/lessons/{$id}/" . $fileName;

            $savedTo = "public/" . $path;

            Storage::put($savedTo, file_get_contents($document));

            return [
                'sts' => 1,
                'html' => '',
                'path' => $path,
                'msg' => 'Materiali u ngarkua me sukses'
            ];

        }

        return [
            'sts' => 0,
            'msg' => 'Materiali nuk mund te ngarkohet'
        ];

    }

    public function download($path)
    {
        $headers = [
            'Content-Type' => 'application/*',
        ];

//        return response()->download(storage_path().'/assets/lessons/test.css', "test.css");
//        echo "aa";die();
        $name = LessonResourceModel::where(LessonResourceClass::TABLE_NAME . '.' . LessonResourceClass::PATH, $path)
            ->first();
        if (file_exists(storage_path() . '/' . $path)) {

            return response()->download(storage_path() . '/' . $path, $name->title, $headers);
        }
//        else{
//            echo "jooo"; die();
//        }
    }
}
