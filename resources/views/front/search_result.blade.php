@extends("layouts.prime")
@section("title")
    UAMD-search
@endsection

@section("styles")
@endsection

@section("content")
<body id="page-top" class="home">

<!-- PAGE WRAP -->
<div id="page-wrap">

@include("layouts.headerMenu")

    <!-- SUB BANNER -->
    <section class="sub-banner sub-banner-search">
        <div class="awe-parallax bg-section1-demo"></div>
        <div class="awe-overlay overlay-color-1"></div>
        <div class="container">
            <div class="sub-banner-content-result">
                <h2>{!! count($courses) !!} Kurse u gjetën për <a href="#">{!! $param !!}</a></h2>
            </div>
        </div>
    </section>
    <!-- END / SUB BANNER -->


    <!-- PAGE CONTROL -->
    <section class="page-control">
        <div class="container">
            <div class="page-info">
                <a href="{!! \Illuminate\Support\Facades\URL::route("home") !!}">
                    <i class="icon md-arrow-left"></i>Back to home</a>
            </div>
            <div class="page-view">
                View
                <span class="page-view-info view-grid active" title="View grid"><i class="icon md-ico-2"></i></span>
                <span class="page-view-info view-list" title="View list"><i class="icon md-ico-1"></i></span>
                <div class="mc-select">
                    <select class="select" name="" id="all-categories">
                        <option value="">All level</option>
                        <option value="">2</option>
                    </select>
                </div>
            </div>
        </div>
    </section>
    <!-- END / PAGE CONTROL -->

    <!-- CATEGORIES CONTENT -->
    <section id="categories-content" class="categories-content">
        <div class="container">
            <div class="content grid">
                <div class="row">
                    @foreach($courses as $course)
                    <!-- ITEM -->
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <div class="mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="{!! $course->image !!}" alt="">
                            </div>
                            <div class="meta-categories"><a href="#">{!! $course->course_name !!}</a></div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="images/avatar-1.jpg" alt="">
                                </div>
                                <h4><a href="{!! route("course_intro", $course->lecture_course_id) !!}">{!! $course->desc !!}</a></h4>
                                <div class="name-author">
                                    <a href="#">{!! $course->title !!} {!! $course->name !!} {!! $course->surname !!}</a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="rating">
                                    @for($j = 0; $j < $course->review; $j++)
                                        <a href="#" class="active"></a>
                                    @endfor

                                    @if($course->review < 5)
                                        @for($i = 0; $i < 5-$course->review; $i++)
                                            <a href="#"></a>
                                        @endfor
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END / ITEM -->
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <!-- END / CATEGORIES CONTENT -->


    @include("layouts.footerMenu")
</div>
<!-- END / PAGE WRAP -->
@include("layouts.footerScripts")
</body>
@endsection
