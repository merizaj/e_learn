<?php

namespace App\Models;

use App\Http\Classes\FacultyClass;
use Illuminate\Database\Eloquent\Model;

class FacultyModel extends Model
{
    protected $table = FacultyClass::TABLE_NAME;
    protected $primaryKey = FacultyClass::ID;
    public $timestamps = true;
}
