@extends("layouts.prime")
@section("title")
    UAMD-learn
@endsection

@section("styles")
    <style>
        /*.kategori a:hover, a:focus {*/
        /*color: #A6A6A6 !important;*/
        /*}*/
    </style>
@endsection

@section("content")
    <body id="page-top" class="home">

    <!-- PAGE WRAP -->
    <div id="page-wrap">

    @include("layouts.headerMenu")

    <!-- SUB BANNER -->
        <section class="sub-banner section">
            <div class="awe-parallax bg-profile-feature"></div>
            <div class="awe-overlay overlay-color-3"></div>
            <div class="container">
                <div class="sub-banner-content">
                    <h2 class="big">Te gjitha kurset</h2>
                    <p>Aksesim i shpejt per cdo material.</p>
                </div>
            </div>
        </section>
        <!-- END / SUB BANNER -->

        <!-- PAGE CONTROL -->
        <section class="page-control">
            <div class="container">
                <div class="page-info">
                    <a href="{!! \Illuminate\Support\Facades\URL::route('home') !!}">
                        <i class="icon md-arrow-left"></i>Ktheu tek Kreu
                    </a>
                </div>
                <div class="page-view">
                    View
                    <span class="page-view-info view-grid active" title="View grid"><i class="icon md-ico-2"></i></span>
                    <span class="page-view-info view-list" title="View list"><i class="icon md-ico-1"></i></span>
                    <div class="mc-select">
                        <select class="select" name="" id="all-categories">
                            <option value="">All level</option>
                            {{--<option value="">2</option>--}}
                        </select>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / PAGE CONTROL -->

        <!-- CATEGORIES CONTENT -->
        <section id="categories-content" class="categories-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-9 col-md-push-3">
                        <div class="content grid">
                            <div class="row">
                            @foreach($courses as $course)
                                <!-- ITEM -->
                                    <div class="col-sm-6 col-md-4">
                                        <div class="mc-item mc-item-2">
                                            <div class="image-heading">
                                                <img src="{!! $course->image !!}" alt="">
                                            </div>
                                            <div class="meta-categories">
                                                <a href="{!! \Illuminate\Support\Facades\URL::route("course_intro",
                                                        $course->lecture_course_id) !!}">
                                                    {!! $course->course_name !!}
                                                </a>
                                            </div>
                                            <div class="content-item">
                                                <div class="image-author">
                                                    <img src="images/avatar-1.jpg" alt="">
                                                </div>
                                                <h4><a href="{!! \Illuminate\Support\Facades\URL::route("course_intro",
                                                        $course->lecture_course_id) !!}">
                                                        {!! mb_substr($course->desc, 0, 255) !!}
                                                    </a>
                                                </h4>
                                                <div class="name-author">
                                                    <a href="javascript:void(0)">
                                                        {!! $course->title !!} {!! $course->name !!} {!! $course->surname !!}
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ft-item">
                                                <div class="rating">
                                                    @for($j = 0; $j < $course->review; $j++)
                                                        <a href="#" class="active"></a>
                                                    @endfor

                                                    @if($course->review < 5)
                                                        @for($i = 0; $i < 5-$course->review; $i++)
                                                            <a href="#"></a>
                                                        @endfor
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END / ITEM -->
                                @endforeach

                            </div>
                        </div>
                    </div>

                    <!-- SIDEBAR CATEGORIES -->
                    <div class="col-md-3 col-md-pull-9">
                        <aside class="sidebar-categories">
                            <div class="inner">

                                <!-- WIDGET TOP -->
                                <div class="widget">
                                    <ul class="list-style-block">
                                        <li class="current"><a href="#">Fakulteti</a></li>
                                        <li><a href="#">Departamentet</a></li>
                                        <li><a href="#">Deget</a></li>
                                        <li><a href="#">Kurset</a></li>
                                    </ul>
                                </div>
                                <!-- END / WIDGET TOP -->

                                <!-- WIDGET CATEGORIES -->
                                <div class="widget widget_categories">
                                    <ul class="list-style-block kategori">
                                        <li><a href="#">Fakulteti</a></li>
                                        <li style="margin-left: 24px">
                                            <a href="#">Teknologji Informacioni</a>
                                        </li>

                                        <li style="margin-left: 24px">
                                            <a href="#">Departamentet</a>
                                        </li>

                                        <li style="margin-left: 48px">
                                            <a href="#">Shkenca Kompjuterike</a>
                                        </li>
                                        <li style="margin-left: 72px"><a href="#">Shkenca Kompjuterike</a></li>
                                        <li style="margin-left: 72px"><a href="#">Informatikë Anglisht</a></li>
                                        <li style="margin-left: 48px">
                                            <a href="#">Matematikë</a>
                                        </li>
                                        <li style="margin-left: 72px"><a href="#">Matematikë Informatikë</a></li>
                                        <li style="margin-left: 48px">
                                            <a href="#">Teknologji Informacioni</a>
                                        </li>
                                        <li style="margin-left: 72px"><a href="#">Teknologji Informacioni</a></li>
                                        <li style="margin-left: 72px"><a href="#">Sisteme Informacioni</a></li>
                                        <li style="margin-left: 72px"><a href="#">Multimedia dhe
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                Televizion Dixhital</a>
                                        </li>

                                    </ul>
                                </div>
                                <!-- END / WIDGET CATEGORIES -->

                                <!-- BANNER ADS -->
                                <div class="mc-banner" style="height: 32px">
                                    {{--<a href="#"><img src="images/banner-ads-1.jpg" alt=""></a>--}}
                                </div>
                                <!-- END / BANNER ADS -->

                                <!-- BANNER ADS -->
                                <div class="mc-banner">
                                    {{--<a href="#"><img src="images/banner-ads-2.jpg" alt=""></a>--}}
                                </div>
                                <!-- END / BANNER ADS -->
                            </div>
                        </aside>
                    </div>
                    <!-- END / SIDEBAR CATEGORIES -->

                </div>
            </div>
        </section>
        <!-- END / CATEGORIES CONTENT -->

        @include("layouts.footerMenu")
    </div>
    <!-- END / PAGE WRAP -->

    @include("layouts.footerScripts")
    </body>
@endsection