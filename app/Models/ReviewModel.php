<?php

namespace App\models;

use App\Http\Classes\ReviewClass;
use Illuminate\Database\Eloquent\Model;

class ReviewModel extends Model
{
    protected $table = ReviewClass::TABLE_NAME;
    protected $primaryKey = ReviewClass::ID;
    public $timestamps = true;
}
