<?php

namespace App\models;

use App\Http\Classes\UserCommentClass;
use Illuminate\Database\Eloquent\Model;

class UserCommentsModel extends Model
{

    protected $table = UserCommentClass::TABLE_NAME;
    protected $primaryKey = UserCommentClass::ID;
    public $timestamps = true;
}
