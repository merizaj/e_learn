<?php

namespace App\Http\Controllers;

use App\Http\Classes\LoginClass;
use App\Http\Classes\Utils;
use App\Models\LoginModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{

    public function login(Request $request)
    {
        $this->validate($request,
            [
                'username' => 'required',
                'password' => 'required',
            ],
            [
                'username.required' => 'Vendosni emrin e përdoruesit!',
                'password.required' => 'Vendosni fjalkalimin!',
            ]);

        if (isset($request->username) && isset($request->password)) {
            try {
                $username = mb_strtolower(htmlentities(trim($request->username)));
                $pass = htmlentities(trim($request->password));

                list($msg, $res, $user) = $this->exists($username, $pass);

                if (!$res) {
                    return Redirect::back()->withErrors($msg);
                }

                    Utils::setLogin( $user->id, $user->role, $user->uname);

                if (!empty(session("lect_course_id"))){
                    return Redirect::route("course_intro", session("lect_course_id"));
                }
                return Redirect::route('home');
            } catch (Exception $e) {

                return Redirect::back()->withErrors($e->getMessage()." - ".$e->getTraceAsString());
                return Redirect::back()->withErrors(Lang::get("errors.user_notfound"));
            }
        }else{
            return Redirect::back()->withErrors(Lang::get("errors.user_notfound"));
        }
    }

    private function exists($username, $pass)
    {
        try {
            $user = LoginModel::where(LoginClass::TABLE_NAME . '.' . LoginClass::UNAME, $username)
                ->where(LoginClass::TABLE_NAME . '.' . LoginClass::STATUS, LoginClass::STATUS_ACTIVE)
                ->first();
            if (!$user) {
                return [Lang::get("errors.user_notfound").'1', false, []];
            }

            if (!Hash::check($pass, $user->upass)) {
                return [Lang::get("errors.user_notfound").'2', false, []];
            }

            return ["", true, json_decode($user)];
        } catch (Exception $e) {
            return [Lang::get("errors.user_notfound").'3', false, []];
        }
    }

}
