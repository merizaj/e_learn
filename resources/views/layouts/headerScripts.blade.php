<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<!-- Google font -->
<link href='{!! asset('css/fonts/font1.css') !!}' rel='stylesheet' type='text/css'>
<link href='{!! asset('css/fonts/font2.css') !!}' rel='stylesheet' type='text/css'>
<!-- Css -->
<link rel="stylesheet" type="text/css" href="{!! asset('css/library/bootstrap.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('css/library/font-awesome.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('css/library/owl.carousel.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('css/md-font.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('css/style.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('css/toastr.min.css') !!}">
<link rel="stylesheet" type="text/css" href="{!! asset('css/swal.min.css') !!}">