<?php

use App\Http\Classes\StudCrsComClass;
use App\Http\Classes\UserCommentClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UserCommentClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(UserCommentClass::ID);
            $table->string(UserCommentClass::COMMENT);
            $table->integer(UserCommentClass::ID_LOGIN);
            $table->integer(UserCommentClass::ID_LECT_COURSE);
            $table->smallInteger(UserCommentClass::STATUS)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
