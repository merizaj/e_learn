
@extends("layouts.prime")
@section("title")
    UAMD-search
@endsection

@section("styles")
@endsection

@section("content")
<body id="page-top" class="home home-two">

<!-- PAGE WRAP -->
<div id="page-wrap">

@include("layouts.headerMenu")


    <!-- SUB BANNER RESULT NOT FOUND -->
    <section class="sub-banner sub-banner-search">
        <div class="awe-parallax bg-section1-demo"></div>
        <div class="awe-overlay overlay-color-1"></div>
        <div class="container">
            <div class="sub-banner-content-result">
                <h2>{!! count($courses) !!} Kurse u gjetën për <a href="#">{!! $param !!}</a></h2>
                <div class="form-item">
                    <input type="text">
                </div>
                <div class="form-actions">
                    <input type="submit" value="Search">
                </div>
            </div>
        </div>
    </section>

    <!-- END / SUB BANNER RESULT NOT FOUND -->


    <!-- PAGE CONTROL -->
    <section class="page-control">
        <div class="container">
            <div class="page-info">
                <a href="{!! \Illuminate\Support\Facades\URL::route("home") !!}">
                    <i class="icon md-arrow-left"></i>Back to home</a>
            </div>
            <div class="page-view">
                {{--View--}}
                {{--<span class="page-view-info view-grid active" title="View grid"><i class="icon md-ico-2"></i></span>--}}
                {{--<span class="page-view-info view-list" title="View list"><i class="icon md-ico-1"></i></span>--}}
                {{--<div class="mc-select">--}}
                    {{--<select class="select" name="" id="all-categories">--}}
                        {{--<option value="">All level</option>--}}
                        {{--<option value="">2</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>
    <!-- END / PAGE CONTROL -->
    <!-- CATEGORIES  -->

    <section class="categories-section" style="padding: 0">
        <div class="container">
            <h3>Or you can try some categories below</h3>
            <div class="widget widget_categories">
                <div class="row">
                    <ul>
                        @foreach($dep as $d)
                        <li class="col-xs-6 col-sm-4 col-md-2">
                            <a href="#">{!! $d->dname !!}</a>
                        </li>
                        @endforeach
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Business &amp;<br> Finance</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Maths &amp;<br> Sciences</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Atrs &amp;<br>Photography</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Craft &amp;<br> Handmade</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Food &amp;<br>Nutrition</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Graphic &amp;<br>Web Design</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Business &amp;<br> Finance</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Maths &amp;<br> Sciences</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Atrs &amp;<br>Photography</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Craft &amp;<br> Handmade</a></li>--}}
                        {{--<li class="col-xs-6 col-sm-4 col-md-2"><a href="#">Food &amp;<br>Nutrition</a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- END / CATEGORIES -->

    <!-- COURSE CONCERN -->
    <section id="course-concern" class="course-concern">
        <div class="container">
            <h3 class="md black">Courses you may concern</h3>
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <!-- MC ITEM -->
                    <div class="mc-item mc-item-2">
                        <div class="image-heading">
                            <img src="images/feature/img-1.jpg" alt="">
                        </div>
                        <div class="meta-categories"><a href="#">Web design</a></div>
                        <div class="content-item">
                            <div class="image-author">
                                <img src="images/avatar-1.jpg" alt="">
                            </div>
                            <h4><a href="course-intro.html">The Complete Digital Photography Course Amazon Top Seller</a></h4>
                            <div class="name-author">
                                By <a href="#">Name of Mr or Mrs</a>
                            </div>
                        </div>
                        <div class="ft-item">
                            <div class="rating">
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#"></a>
                                <a href="#"></a>
                            </div>
                            <div class="view-info">
                                <i class="icon md-users"></i>
                                2568
                            </div>
                            <div class="comment-info">
                                <i class="icon md-comment"></i>
                                25
                            </div>
                            <div class="price">
                                $190
                                <span class="price-old">$134</span>
                            </div>
                        </div>
                    </div>
                    <!-- END / MC ITEM -->
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3">
                    <!-- MC ITEM -->
                    <div class="mc-item mc-item-2">
                        <div class="image-heading">
                            <img src="images/feature/img-1.jpg" alt="">
                        </div>
                        <div class="meta-categories"><a href="#">Web design</a></div>
                        <div class="content-item">
                            <div class="image-author">
                                <img src="images/avatar-1.jpg" alt="">
                            </div>
                            <h4><a href="course-intro.html">The Complete Digital Photography Course Amazon Top Seller</a></h4>
                            <div class="name-author">
                                By <a href="#">Name of Mr or Mrs</a>
                            </div>
                        </div>
                        <div class="ft-item">
                            <div class="rating">
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#"></a>
                                <a href="#"></a>
                            </div>
                            <div class="view-info">
                                <i class="icon md-users"></i>
                                2568
                            </div>
                            <div class="comment-info">
                                <i class="icon md-comment"></i>
                                25
                            </div>
                            <div class="price">
                                $190
                                <span class="price-old">$134</span>
                            </div>
                        </div>
                    </div>
                    <!-- END / MC ITEM -->
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3">
                    <!-- MC ITEM -->
                    <div class="mc-item mc-item-2">
                        <div class="image-heading">
                            <img src="images/feature/img-1.jpg" alt="">
                        </div>
                        <div class="meta-categories"><a href="#">Web design</a></div>
                        <div class="content-item">
                            <div class="image-author">
                                <img src="images/avatar-1.jpg" alt="">
                            </div>
                            <h4><a href="course-intro.html">The Complete Digital Photography Course Amazon Top Seller</a></h4>
                            <div class="name-author">
                                By <a href="#">Name of Mr or Mrs</a>
                            </div>
                        </div>
                        <div class="ft-item">
                            <div class="rating">
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#"></a>
                                <a href="#"></a>
                            </div>
                            <div class="view-info">
                                <i class="icon md-users"></i>
                                2568
                            </div>
                            <div class="comment-info">
                                <i class="icon md-comment"></i>
                                25
                            </div>
                            <div class="price">
                                $190
                                <span class="price-old">$134</span>
                            </div>
                        </div>
                    </div>
                    <!-- END / MC ITEM -->
                </div>

                <div class="col-xs-6 col-sm-4 col-md-3">
                    <!-- MC ITEM -->
                    <div class="mc-item mc-item-2">
                        <div class="image-heading">
                            <img src="images/feature/img-1.jpg" alt="">
                        </div>
                        <div class="meta-categories"><a href="#">Web design</a></div>
                        <div class="content-item">
                            <div class="image-author">
                                <img src="images/avatar-1.jpg" alt="">
                            </div>
                            <h4><a href="course-intro.html">The Complete Digital Photography Course Amazon Top Seller</a></h4>
                            <div class="name-author">
                                By <a href="#">Name of Mr or Mrs</a>
                            </div>
                        </div>
                        <div class="ft-item">
                            <div class="rating">
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#" class="active"></a>
                                <a href="#"></a>
                                <a href="#"></a>
                            </div>
                            <div class="view-info">
                                <i class="icon md-users"></i>
                                2568
                            </div>
                            <div class="comment-info">
                                <i class="icon md-comment"></i>
                                25
                            </div>
                            <div class="price">
                                $190
                                <span class="price-old">$134</span>
                            </div>
                        </div>
                    </div>
                    <!-- END / MC ITEM -->
                </div>
            </div>
        </div>
    </section>
    <!-- END / COURSE CONCERN -->

    @include("layouts.footerMenu")

</div>
<!-- END / PAGE WRAP -->
@include("layouts.footerScripts")

</body>
@endsection
