@extends("layouts.prime")
@section("title")
    UAMD-learn
@endsection

@section("styles")

    <style>

    </style>
@endsection

@section("content")
    <body id="page-top">

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- PRELOADER -->
        <div id="preloader">

        </div>
        <!-- END / PRELOADER -->
    @include("layouts.headerMenu")

    <!-- SUB BANNER -->
        <section class="sub-banner sub-banner-course">
            <div class="awe-static bg-sub-banner-course"></div>
            <div class="container">
                <div class="sub-banner-content">
                    <h2 class="text-center">{!! $course->course_name !!}</h2>
                </div>
            </div>
        </section>
        <!-- END / SUB BANNER -->

        <!-- COURSE -->
        <section class="course-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="sidebar-course-intro">
                            <div class="breadcrumb">
                                <a href="#">Kryefaqja</a> /
                                <a href="#">Kurset</a> /
                                {!! $course->course_name !!}
                            </div>
                            <div class="video-course-intro">
                                <div class="inner">
                                    <div class="video-place">
                                        <div class="img-thumb">
                                            <img src="{!! asset($course->image) !!}" alt="">
                                        </div>
                                        <div class="awe-overlay"></div>
                                    </div>
                                    <div class="video embed-responsive embed-responsive-16by9">
                                        <iframe src="#"
                                                class="embed-responsive-item">
                                        </iframe>
                                    </div>
                                </div>
                                <div class="price" style="height: 32px">
                                </div>
                                @if($isMine && \App\Http\Classes\Utils::isStudent()
                                            && !$isAttending)
                                    <a type="button" id="sts_course" class=" mc-btn btn-style-1 follow">Ndiq kursin</a>
                                @elseif($isMine && \App\Http\Classes\Utils::isLecturer())
                                    <a href="{!! \Illuminate\Support\Facades\URL::route("edit_course", $course->lect_course_id) !!}"
                                       type="button" id="sts_course" class=" mc-btn btn-style-1 edit">Përditëso
                                        Kursin</a>
                                @else
                                    <a href="#" id="sts_course"></a>
                                @endif
                            </div>

                            <div class="new-course">
                                <div class="item course-code">
                                    <i class="icon md-barcode"></i>
                                    <h4><a href="#">Viti</a></h4>
                                    <p class="detail-course">{!! $course->viti !!}</p>
                                </div>
                                <div class="item course-code">
                                    <i class="icon md-time"></i>
                                    <h4><a href="#">Kohëzgjatja 11</a></h4>
                                    <p class="detail-course">{!! $course->duration !!}</p>
                                </div>
                                <div class="item course-code">
                                    <i class="icon md-img-check"></i>
                                    <h4><a href="#">Datë fillimi</a></h4>
                                    <p class="detail-course">{!! $course->start_at !!}</p>
                                </div>
                            </div>
                            <hr class="line">
                            <div class="about-instructor">
                                <h4 class="xsm black bold">Lektori</h4>
                                <ul>
                                    <li>
                                        <div class="image-instructor text-center">
                                            {{--<img src="{!! asset() !!}images/team-13.jpg" alt="">--}}
                                        </div>
                                        <div class="info-instructor">
                                            <cite class="sm black">
                                                <a href="#">
                                                    {!! $course->title !!}
                                                    {!! $course->name  !!}
                                                    {!! $course->surname !!}
                                                </a></cite>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-envelope"></i></a>
                                            <a href="#"><i class="fa fa-check-square"></i></a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <hr class="line">

                            <div class="widget widget_share">
                                <i class="icon md-forward"></i>
                                <h4 class="xsm black bold">Share course</h4>
                                <div class="share-body">
                                    <a href="#" class="twitter" title="twitter">
                                        <i class="icon md-twitter"></i>
                                    </a>
                                    <a href="#" class="pinterest" title="pinterest">
                                        <i class="icon md-pinterest-1"></i>
                                    </a>
                                    <a href="#" class="facebook" title="facebook">
                                        <i class="icon md-facebook-1"></i>
                                    </a>
                                    <a href="#" class="google-plus" title="google plus">
                                        <i class="icon md-google-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="tabs-page">
                            <ul class="nav-tabs" role="tablist">
                                <li class="active">
                                    <a href="#introduction" role="tab" data-toggle="tab">Hyrje</a>
                                </li>
                                @if(\App\Http\Classes\Utils::isStudent() && $isAttending)
                                    <li id="lessons_tab">
                                        <a href="#outline" role="tab" data-toggle="tab">Leksionet</a>
                                    </li>
                                    <li><a href="#review" role="tab" id="review_tab" data-toggle="tab">Review</a></li>
                                @elseif(\App\Http\Classes\Utils::isLecturer() && $isMine)
                                    <li id="lessons_tab">
                                        <a href="#outline" role="tab" data-toggle="tab">Leksionet</a>
                                    </li>
                                    <li><a href="#review" role="tab" id="review_tab" data-toggle="tab">Review</a></li>
                                @endif
                                @if(\App\Http\Classes\Utils::isLecturer() && $isMine)
                                    <li><a href="#student" role="tab" data-toggle="tab">Studenti</a></li>
                                @endif
                                <li><a href="#conment" role="tab" id="comment" data-toggle="tab">Komente</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- INTRODUCTION -->
                                <div class="tab-pane fade in active" id="introduction">
                                    <h4 class="sm black bold">Përshkrim</h4>
                                    <p>{!! $course->desc !!}
                                    </p>
                                    <h4 class="sm black bold">Objektivat</h4>
                                    <ul class="list-disc">
                                        <li><p>Objektivat e lendes </p></li>
                                        <li><p>Objektivat e lendes</p></li>
                                    </ul>
                                </div>
                                <!-- END / INTRODUCTION -->

                                <!-- OUTLINE -->
                                <div class="tab-pane fade" id="outline">

                                    @if($nr_lexione == 0)
                                        <div class="section-outline">
                                            <h4 class="tit-section xsm">Nuk ka leksione për këtë lëndë</h4>
                                        </div>
                                    @endif
                                    @foreach($lexion as $l)
                                    <!-- SECTION OUTLINE -->
                                        <div class="section-outline">
                                            <h4 class="tit-section xsm">{!! $l["title"] !!}</h4>
                                            <ul class="section-list">
                                                @for($i = 0; $i< count($l["item"]); $i++)
                                                    <li>
                                                        <div class="count"><span>{!! $i+1 !!}</span></div>
                                                        <div class="list-body">
                                                            <i class="icon md-gallery-2"></i>
                                                            <p><a href="#">{!! $l["item"][$i]->title !!}</a></p>
                                                            <div class="data-lessons">
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <a href="{!! \Illuminate\Support\Facades\URL::route("download",
                                                            $l["item"][$i]->path
                                                            )!!}"
                                                           download
                                                           class="mc-btn-2 btn-style-2 shkarko_file">Shkarko</a>
                                                        {{--$l["item"][$i]->path,--}}
                                                        {{--$l["item"][$i]->title) !!}"--}}

                                                    </li>
                                                @endfor
                                            </ul>
                                        </div>
                                        <!-- END / SECTION OUTLINE -->
                                    @endforeach
                                </div>
                                <!-- END / OUTLINE -->

                                <!-- REVIEW -->
                                <div class="tab-pane fade" id="review">
                                    {{--<div class="row">--}}
                                    {{--<div class="col-md-12">--}}
                                    <div class="total-review">
                                        <h3 class="md black review_nr"> 0 Review</h3>
                                        <div class="rating total_rate">
                                        </div>
                                    </div>
                                    {{--</div>--}}
                                    {{--<div class="col-md-12">--}}
                                    @if(\App\Http\Classes\Utils::isStudent() && $isAttending)
                                        <div class="body-review" style="margin-top: 24px">
                                            <div class="content-review">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-submit">
                                                            <input type="button" id="saveReview" value="Review"
                                                                   class="mc-btn-2 btn-style-2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8" style="text-align: end;">
                                                        <div class="comment-form-comment">
                                                            <div class="rating review_stars">
                                                                <a href="javascript:void(0)" class="star"
                                                                   data-revnr="1"></a>
                                                                <a href="javascript:void(0)" class="star"
                                                                   data-revnr="2"></a>
                                                                <a href="javascript:void(0)" class="star"
                                                                   data-revnr="3"></a>
                                                                <a href="javascript:void(0)" class="star"
                                                                   data-revnr="4"></a>
                                                                <a href="javascript:void(0)" class="star"
                                                                   data-revnr="5"></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                    <ul class="list-review">

                                    </ul>
                                </div>
                                <!-- END / REVIEW -->

                                <!-- STUDENT -->
                                <div class="tab-pane fade" id="student">
                                    <h3 class="md black">{!! count($students) !!} student</h3>
                                    <div class="tab-list-student">
                                        <ul class="list-student">
                                            <!-- LIST STUDENT -->
                                            @foreach($students as $student)
                                                <li>
                                                    <div class="image">
                                                        <img src="images/team-13.jpg" alt="">
                                                    </div>
                                                    <div class="list-body">
                                                        <cite class="xsm"><a
                                                                    href="#">{!! $student->name !!} {!! $student->surname !!}</a></cite>
                                                        <span class="address">Durres</span>
                                                        <div class="icon-wrap">
                                                            <a href="#"><i class="icon md-email"></i></a>
                                                            {{--<a href="#"><i class="icon md-user-plus"></i></a>--}}
                                                        </div>
                                                    </div>
                                                </li>
                                        @endforeach
                                        <!-- END / LIST STUDENT -->
                                        </ul>
                                    </div>
                                    <div class="load-more">
                                        {{--<a href="#">--}}
                                        {{--<i class="icon md-time"></i>--}}
                                        {{--Load more previous update</a>--}}
                                    </div>
                                </div>
                                <!-- END / STUDENT -->


                                <!-- COMMENT -->
                                <div class="tab-pane fade" id="conment">
                                    <div id="respond">
                                        <h3 class="md black comment_nr"></h3>

                                        <div class="comment-form-comment">
                                                <textarea
                                                        name="coment"
                                                        id="content"
                                                        placeholder="Komento..."></textarea>
                                        </div>
                                        <div class="form-submit">
                                            <input type="button" id="saveComent" value="Komento"
                                                   class="mc-btn-2 btn-style-2">
                                        </div>
                                    </div>
                                    <ul class="commentlist">

                                    </ul>
                                </div>
                                <!-- END / COMMENT -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / COURSE TOP -->

        @include("layouts.footerMenu")
    </div>
    <!-- END / PAGE WRAP -->

    @include("layouts.footerScripts")

    <script>

        $(document).ready(function () {

            @if(\App\Http\Classes\Utils::isStudent())
            $("#sts_course").click(function () {
                $.ajax({
                    type: 'POST',
                    url: '{!! \Illuminate\Support\Facades\URL::route("ndiq_kursin") !!}',
                    data: {_token: '{!! csrf_token() !!}', id: '{!! $course->lect_course_id !!}'},
                    success: function (data) {
                        if (data.status == 1) {
                            window.location.reload();
                        }
                    }
                })
            });

            @endif

            $("#comment").click(function () {
                $.ajax({
                    url: '{!! route("course_comments") !!}',
                    type: 'POST',
                    data: {_token: '{!! csrf_token() !!}', id: '{!! $course->lect_course_id !!}'},
                    success: function (response) {
                        if (response.status == 1) {

                            var html = response.data;
                            $(".comment_nr").html(response.commentnr + ' Komente');
                            $(".commentlist").html(html);
                        }
                    }
                })
            });

            $("#review_tab").click(function () {
                $.ajax({
                    url: '{!! route("course_review") !!}',
                    type: 'POST',
                    data: {_token: '{!! csrf_token() !!}', id: '{!! $course->lect_course_id !!}'},
                    success: function (response) {
                        if (response.status == 1) {

                            var html = response.data;
                            $(".review_nr").html(response.reviewsnr + ' Reviews');
                            $(".list-review").html(html);
                            $(".total_rate").html(response.tot_rate_html);
                        }
                    }
                })
            })
        });


        $("#saveComent").click(function () {
            var content = $("#content");
            var innerListHtml = $(".commentlist").html();
            console.log(content.val());
            console.log(innerListHtml);

            $.ajax({
                url: '{!! route("save_comment") !!}',
                type: 'POST',
                data: {
                    _token: '{!! csrf_token() !!}',
                    id: '{!! $course->lect_course_id !!}',
                    coment: content.val()
                },
                success: function (response) {
                    if (response.status == 1) {

                        console.log(response.html);
                        innerListHtml = response.html + innerListHtml;
                        $(".commentlist").html(innerListHtml);
                        content.val("");
                    } else {

                        content.css({'border': '1px solid red'});
                    }
                }
            })
        });

        var star_chosen = 0;
        $(".star").on("click", function () {

            star_chosen = $(this).data("revnr");
            console.log(star_chosen);

            $(".star").each(function (index, val) {

                if (star_chosen >= $(val).data("revnr")) {
                    console.log($(val).data("revnr"));

                    $(val).addClass("active");
                } else {
                    $(val).removeClass("active");
                }
            })
        });

        $("#saveReview").click(function () {
            var content = star_chosen;
            var innerListHtml = $(".list-review").html();

            console.log(innerListHtml);

            $.ajax({
                url: '{!! route("save_review") !!}',
                type: 'POST',
                data: {
                    _token: '{!! csrf_token() !!}',
                    id: '{!! $course->lect_course_id !!}',
                    review: star_chosen
                },
                success: function (response) {
                    if (response.status == 1) {

                        $(".star").removeClass("active");
                        console.log(response.html);
                        innerListHtml = response.html + innerListHtml;
                        $(".list-review").html(innerListHtml);
                    }
                }
            })
        });

    </script>
    </body>
@endsection