
<!-- FOOTER -->
<footer id="footer" class="footer">
    <div class="first-footer">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <div class="widget megacourse">
                        <h6 class="md">Universiteti <br> Aleksander Moisiu</h6>
                        <p>L.1. Rruga e Currilave, Durres<br>
                           Tel: 00355(0)52 239 161<br>
                           Fax: 00355(0)52 239 163 <br>
                           Email: info@uamd.edu.al</p>
                    </div>
                </div>

                <div class="col-md-3 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget quick_link">
                                <a href="http://aula.uamd.edu.al/" class="mc-btn col-md-10 btn-style-1">Zona Studentit</a>
                                <a href="http://aula.uamd.edu.al/admin" class="mc-btn col-md-10 btn-style-1">Zona Pedagogut</a>
                                <a href="http://www.uamd.edu.al/newuni/index.php/en/" class="mc-btn col-md-10 btn-style-1">Faqja UAMD</a>
                            </div>
                        </div>

                    </div>

                </div>


                <div class="col-md-4">
                    <div class="widget news_letter">
                        <div class="awe-static bg-news_letter"></div>
                        <div class="overlay-color-3"></div>
                        <div class="inner">
                            <div class="letter-heading">
                                <h3 class="md">Na kontaktoni</h3>
                                <p>Per cdo kerkese ndaj platformes kontaktoni ketu!</p>
                            </div>
                            <div class="letter">
                                <form>
                                    <input class="input-text" type="text">
                                    <input type="submit" value="Dergo" class="mc-btn btn-style-2">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="second-footer">
        <div class="container">
            <div class="contact">
                <div class="email">
                    <i class="icon md-email"></i>
                    <a href="#">
                        <span class="__cf_email__" data-cfemail="b4d7dbc1c6c7d1f4d9d1d3d5d0c6c1c4d5d89ad7dbd9">info@uamd.edu.al</span></a>
                </div>
                <div class="phone">
                    <i class="fa fa-mobile"></i>
                    <span>+060 000 1111</span>
                </div>
                <div class="address">
                    <i class="fa fa-map-marker"></i>
                    <span>Durrës, 2018</span>
                </div>
            </div>
            <p class="copyright">Copyright © 2018 Marsela Leshija.</p>
        </div>
    </div>
</footer>
<!-- END / FOOTER -->