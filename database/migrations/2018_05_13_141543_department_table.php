<?php

use App\Http\Classes\DepartmentClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DepartmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DepartmentClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(DepartmentClass::ID);
            $table->string(DepartmentClass::NAME);
            $table->integer(DepartmentClass::ID_FACULTY);
            $table->smallInteger(DepartmentClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
