<?php

use App\Http\Classes\LoginClass;
use App\Models\LoginModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class login_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //region login_users_dummy
        $new1= new LoginModel();
        $new1->uname = "DRFTIA003411";
        $new1->upass = Hash::make("123456");
        $new1->role = LoginClass::STUDENT;
        $new1->status = LoginClass::STATUS_ACTIVE;
        $new1->save();

        $student = new \App\Models\StudentModel();
        $student->name = "student";
        $student->surname = "1";
        $student->nr_matrikull = "DRFTIA003411";
        $student->email = "test@example.com";
        $student->cel = "00";
        $student->title = \App\Http\Classes\StudentClass::BACHELOR;
        $student->viti = 1;
        $student->id_login = $new1->id;
        $student->id_department = 1;
        $student->status = \App\Http\Classes\StudentClass::STATUS_ACTIVE;
        $student->save();

        $new2= new LoginModel();
        $new2->uname = "DRFTIA003412";
        $new2->upass = Hash::make("123456");
        $new2->role = LoginClass::STUDENT;
        $new2->status = LoginClass::STATUS_ACTIVE;
        $new2->save();

        $student2 = new \App\Models\StudentModel();
        $student2->name = "student";
        $student2->surname = "2";
        $student2->nr_matrikull = "DRFTIA003412";
        $student2->email = "test@example.com";
        $student2->cel = "00";
        $student2->title = \App\Http\Classes\StudentClass::BACHELOR;
        $student2->viti = 1;
        $student2->id_login = $new2->id;
        $student2->id_department = 2;
        $student2->status = \App\Http\Classes\StudentClass::STATUS_ACTIVE;
        $student2->save();

        $new3= new LoginModel();
        $new3->uname = "DRFTIA003413";
        $new3->upass = Hash::make("123456");
        $new3->role = LoginClass::STUDENT;
        $new3->status = LoginClass::STATUS_ACTIVE;
        $new3->save();

        $student3 = new \App\Models\StudentModel();
        $student3->name = "student";
        $student3->surname = "3";
        $student3->nr_matrikull = "DRFTIA003413";
        $student3->email = "test@example.com";
        $student3->cel = "00";
        $student3->title = \App\Http\Classes\StudentClass::BACHELOR;
        $student3->viti = 1;
        $student3->id_login = $new3->id;
        $student3->id_department = 3;
        $student3->status = \App\Http\Classes\StudentClass::STATUS_ACTIVE;
        $student3->save();

        //login users
//        $new2= new LoginModel();
//        $new2->uname = "profesor";
//        $new2->upass = Hash::make("123456");
//        $new2->role = LoginClass::LECTURER;
//        $new2->status = LoginClass::STATUS_ACTIVE;
//        $new2->save();
        //endregion

    }
}
