<?php

namespace App\Http\Middleware;

use App\Http\Classes\Utils;
use Closure;
use Illuminate\Support\Facades\Redirect;

class isLogged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Utils::isLoggedIn()){
            return Redirect::route("login");
        }
        return $next($request);
    }
}
