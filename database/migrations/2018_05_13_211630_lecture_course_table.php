<?php

use App\Http\Classes\LectureCourseClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LectureCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LectureCourseClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(LectureCourseClass::ID);
            $table->string(LectureCourseClass::ID_COURSE);
            $table->string(LectureCourseClass::ID_LECTURE);
            $table->string(LectureCourseClass::TYPE)->nullable();
            $table->smallInteger(LectureCourseClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
