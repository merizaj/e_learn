<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:04 PM
 */

namespace App\Http\Classes;


class CourseClass
{
    const TABLE_NAME = "course";
    const ID = "id";
    const NAME = "title";
    const DESC = "desc";
    const IMAGE = "image";
    const DURATION = "duration";
    const START = "start_at";
    const ID_DEPARTMENT = "id_department";
    const VITI = "viti";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}