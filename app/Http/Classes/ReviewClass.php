<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 7/7/2018
 * Time: 11:51 PM
 */

namespace App\Http\Classes;


class ReviewClass
{

    const TABLE_NAME = "stud_review";
    const ID = "id";
    const REVIEW = "review";
    const ID_LOGIN= "id_login";
    const ID_LECT_COURSE = "id_lect_course";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}