@extends("layouts.prime")
@section("title")
    UAMD-learn
@endsection

@section("styles")
    <style>
        .unit_content {
            display: block;
        }

        .item_material_upload {
            display: none;
        }

        .open_body {
            border: 1px solid #fff;
            width: 22px;
            height: 22px;
            line-height: 20px;
            text-align: center;
        }

        .open_body i {
            width: 20px;
        }

        .open_body i:before {
            color: white;
        }
    </style>
@endsection

@section("content")
    <body id="page-top">

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- PRELOADER -->
        <div id="preloader">
        </div>
        <!-- END / PRELOADER -->

    @include("layouts.headerMenu")

    <!-- SUB BANNER -->
        <section class="sub-banner sub-banner-course">
            <div class="awe-static bg-sub-banner-course"></div>
            <div class="container">
                <div class="sub-banner-content">
                    <h2 class="text-center">Universiteti "Aleksandër Moisiu"</h2>
                </div>
            </div>
        </section>
        <!-- END / SUB BANNER -->


        <!-- BLOG -->
        <section class="blog">

            <div class="container">
                <div class="row">

                    <!-- BLOG LIST -->
                    <div class="col-md-8">
                        <div class="blog-single-content">
                            <!-- POST -->
                            <div class="post post-single">
                                <div class="post-title">
                                    <h1 class="big">Rreth Platformes E-learning</h1>
                                </div>

                                <div class="post-media">
                                    <div class="video embed-responsive embed-responsive-16by9">
                                        <iframe width="420" height="315"
                                                src="https://www.youtube.com/embed/-fQLiN9LtWU"
                                                frameborder="0" allowfullscreen></iframe>
                                        {{--<iframe src="https://www.youtube.com/watch?v=-fQLiN9LtWU" class="embed-responsive-item">--}}
                                        {{--</iframe>--}}
                                    </div>
                                </div>

                                <div class="post-content">
                                    <p> Kjo platforme E-Learning mundeson lehtesim ne bashkeveprimin
                                        student-pedagog.

                                        Ajo i jep mundesi si pedagogut edhe studentit te logohen.
                                        Pedagogu e perdor ate per te ngarkuar materiale
                                        te kursit qe jep per stuentet e tij.
                                        Nga ana tjeter studentit i lejohet te aksesoj
                                        matariale te pedagogve te fakultetit te tij. </p>
                                    <h4>Perparesite </h4>
                                    <ol style="list-style: decimal; ">
                                        <li> Lehtesi per aksesimin e materialeve nga studentet.</li>
                                        <li> Lehtesi per ngarkimin e materialeve nga pedagoget.</li>
                                        <li> Ruajtje dhe siguri me e larte e materialeve nga humbja apo mos-aksesimi ne
                                            kohen e duhur.
                                        </li>
                                        <li> Komunitet me interaktiv dhe dinamik, mund te diskutojne dhe te bejne pyetje
                                            dhe mund te ndiqet ne vazhdimesi progresi i cdo studenti.
                                        </li>

                                    </ol>
                                    <h4>Se shpejti!</h4>

                                            <p>Krijimi i nje paneli qe mundeson nderveprimin e sekretarise ne editime te
                                                ndryshime.<br>
                                                Aplikimi i klasave virtuale.</p>

                                    <!-- END / POST -->
                                </div>
                            </div>
                            <!-- END / BLOG LIST -->

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / BLOG -->

        @include("layouts.footerMenu")

    </div>
    <!-- END / PAGE WRAP -->

    @include("layouts.footerScripts")

    </body>
@endsection