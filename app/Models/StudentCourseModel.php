<?php

namespace App\Models;

use App\Http\Classes\StudentCourseClass;
use Illuminate\Database\Eloquent\Model;

class StudentCourseModel extends Model
{
    protected $table = StudentCourseClass::TABLE_NAME;
    protected $primaryKey = StudentCourseClass::ID;
    public $timestamps = true;
}
