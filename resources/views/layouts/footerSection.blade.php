222<footer class="section novi-background bg-cover">

    <div class="container">
        <div class="row-flex">

            <div class="right-block">
                <div class="social-footer">
                    <ul class="social-footer-list">
                        <li>
                            <a href="index.html#">
                                <span aria-hidden="true" class="ei social_facebook_square novi-icon"></span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span aria-hidden="true" class="ei social_twitter_square novi-icon"></span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span aria-hidden="true" class="ei social_pinterest_square novi-icon"></span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span aria-hidden="true" class="ei social_googleplus_square novi-icon"></span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span aria-hidden="true" class="ei social_youtube_square novi-icon"></span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html#">
                                <span aria-hidden="true" class="ei social_skype_square novi-icon"></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="footer-menu">
                    <ul class="footer-menu-list">
                        <li>
                            <a href="index.html#">Home</a>
                        </li>
                        <li>
                            <a href="index.html#">Pages</a>
                        </li>
                        <li>
                            <a href="index.html#">About</a>
                        </li>
                        <li>
                            <a href="index.html#">Services</a>
                        </li>
                        <li>
                            <a href="index.html#">Blog</a>
                        </li>
                        <li>
                            <a href="index.html#">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="left-block">
                <div class="footer-logo">
                    <a href="index.html" class="logo2">
                        <img src="images/logo-white.png" alt="" class="img-responsive"> </a>
                </div>
                <div><span>&copy;&nbsp; </span><span class="copyright-year"></span><span>&nbsp;</span><span>Autozone</span><span>.&nbsp;</span></div>
            </div>
        </div>

    </div>

</footer>