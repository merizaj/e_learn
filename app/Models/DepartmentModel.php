<?php

namespace App\Models;

use App\Http\Classes\DepartmentClass;
use Illuminate\Database\Eloquent\Model;

class DepartmentModel extends Model
{
    protected $table = DepartmentClass::TABLE_NAME;
    protected $primaryKey = DepartmentClass::ID;
    public $timestamps = true;
}
