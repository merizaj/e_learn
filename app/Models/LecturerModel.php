<?php

namespace App\Models;

use App\Http\Classes\LecturerClass;
use Illuminate\Database\Eloquent\Model;

class LecturerModel extends Model
{
    protected $table = LecturerClass::TABLE_NAME;
    protected $primaryKey = LecturerClass::ID;
    public $timestamps = true;
}
