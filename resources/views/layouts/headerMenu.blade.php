<!-- HEADER -->
<header id="header" class="header">
    <div class="container">

        <!-- LOGO -->
        <div class="logo">
            <a href="{!! \Illuminate\Support\Facades\URL::route('home') !!}">
                <img style="max-width: 42px" src="{!! asset("images/uamd_round.png") !!}" alt="">
            </a>
        </div>
        <!-- END / LOGO -->

        <!-- NAVIGATION -->
        <nav class="navigation">

            <div class="open-menu">
                <span class="item item-1"></span>
                <span class="item item-2"></span>
                <span class="item item-3"></span>
            </div>

            <!-- MENU -->
            <ul class="menu">
                <li>
                    <a href="{!! \Illuminate\Support\Facades\URL::route('home') !!}">
                        Kreu
                    </a>
                </li>
                <li><a href="{!! \Illuminate\Support\Facades\URL::route('about') !!}">
                        Rreth nesh
                    </a>
                <li><a href="{!! \Illuminate\Support\Facades\URL::route('course_list') !!}">Kurset</a></li>
                @if(!\App\Http\Classes\Utils::isLoggedIn())
                <li class="menu-item-has-children">
                    <a href="{!! \Illuminate\Support\Facades\URL::route('login') !!}">Hyr</a>
                </li>
                @endif
            </ul>
            <!-- END / MENU -->

            <!-- SEARCH BOX -->
            <div class="search-box">
                <i class="icon md-search"></i>
                <div class="search-inner">
                    <form>
                        <input type="text" placeholder="kerko">
                    </form>
                </div>
            </div>
            <!-- END / SEARCH BOX -->

        @if(\App\Http\Classes\Utils::isLoggedIn())
            <!-- LIST ACCOUNT INFO -->
                <ul class="list-account-info">

                    <!-- MESSAGE INFO -->
                    {{--<li class="list-item messages">--}}
                        {{--<div class="message-info item-click">--}}
                            {{--<i class="icon md-email"></i>--}}
                            {{--<span class="itemnew"></span>--}}
                        {{--</div>--}}

                    {{--</li>--}}
                    <!-- END / MESSAGE INFO -->

                    <!-- NOTIFICATION -->
                    {{--<li class="list-item notification">--}}
                        {{--<div class="notification-info item-click">--}}
                            {{--<i class="icon md-bell"></i>--}}
                            {{--<span class="itemnew"></span>--}}
                        {{--</div>--}}

                    {{--</li>--}}
                    <!-- END / NOTIFICATION -->

                    <li class="list-item account">
                        <div class="account-info item-click">
                            <img src="{!! asset("images/user.svg") !!}" alt="">
                        </div>
                        <div class="toggle-account toggle-list">
                            <ul class="list-account">
                                <li>
                                    <a href="{!! \Illuminate\Support\Facades\URL::route("profile") !!}">
                                        <i class="icon  md-user-minus" ></i>Profil</a>
                                </li>
                                <li>
                                    <a href="{!! \Illuminate\Support\Facades\URL::route("login") !!}">
                                        <i class="icon md-arrow-right"></i>Dil</a>
                                </li>
                            </ul>
                        </div>
                    </li>


                </ul>
                {{--<!-- END / LIST ACCOUNT INFO -->--}}
            @endif

        </nav>
        <!-- END / NAVIGATION -->

    </div>
</header>
<!-- END / HEADER -->