<?php

namespace App\Http\Controllers;

use App\Http\Classes\CourseClass;
use App\Http\Classes\DegaClass;
use App\Http\Classes\DepartmentClass;
use App\Http\Classes\LectureCourseClass;
use App\Http\Classes\LecturerClass;
use App\Http\Classes\ReviewClass;
use App\Http\Classes\StudentClass;
use App\Http\Classes\StudentCourseClass;
use App\Http\Classes\Utils;
use App\Models\CourseModel;
use App\models\ReviewModel;
use App\Models\StudentCourseModel;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class StudentController extends Controller
{

    /**
     * @return $courses
     * Kthen listen e gjith kurseve qe ndjek studenti
     */
    public function index()
    {
        try {

            $courses = DB::table(StudentCourseClass::TABLE_NAME)
                ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name ",
                    CourseClass::TABLE_NAME . '.' . CourseClass::ID . " as course_id ",
                    LectureCourseClass::TABLE_NAME.'.'.LectureCourseClass::ID.' as lecture_course_id',
                    CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::DESC,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                    LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)
                ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID,
                    StudentCourseClass::TABLE_NAME . '.' . StudentCourseClass::ID_LECTURE_COURSE)
                ->join(CourseClass::TABLE_NAME, CourseClass::TABLE_NAME . '.' . CourseClass::ID,
                    StudentCourseClass::TABLE_NAME . '.' . StudentCourseClass::ID)
                ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                    LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
                ->where(StudentCourseClass::TABLE_NAME . '.' . StudentCourseClass::ID, '=', Utils::getUserId())
                ->where(StudentCourseClass::TABLE_NAME . '.' . StudentCourseClass::STATUS, '=', StudentClass::STATUS_ACTIVE)
                ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE)
                ->get();


            return view("front.student_courses")
                ->with("courses", $courses);

        } catch (Exception $e) {

            return Redirect::back();

        }
    }

    /**
     * @return $courses
     * Kthen gjith listen e kurseve te departamentit
     */
    public function listAllCourses()
    {

        $courses = DB::table(CourseClass::TABLE_NAME)
            ->select(CourseClass::TABLE_NAME . '.' . CourseClass::NAME . " as course_name", CourseClass::TABLE_NAME . '.' . CourseClass::ID,
                LectureCourseClass::TABLE_NAME.'.'.LectureCourseClass::ID.' as lecture_course_id',
                CourseClass::TABLE_NAME . '.' . CourseClass::IMAGE, CourseClass::TABLE_NAME . '.' . CourseClass::DESC,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::TITLE, LecturerClass::TABLE_NAME . '.' . LecturerClass::NAME,
                LecturerClass::TABLE_NAME . '.' . LecturerClass::SURNAME)
            ->join(DepartmentClass::TABLE_NAME, DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID,
                CourseClass::ID_DEPARTMENT)
            ->join(StudentClass::TABLE_NAME, StudentClass::TABLE_NAME . '.' . StudentClass::ID_DEPARTMENT,
                DepartmentClass::TABLE_NAME . '.' . DepartmentClass::ID)
            ->join(LectureCourseClass::TABLE_NAME, LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_COURSE,
                CourseClass::TABLE_NAME . '.' . CourseClass::ID)
            ->join(LecturerClass::TABLE_NAME, LecturerClass::TABLE_NAME . '.' . LecturerClass::ID,
                LectureCourseClass::TABLE_NAME . '.' . LectureCourseClass::ID_LECTURE)
            ->where(StudentClass::TABLE_NAME . '.' . StudentClass::ID, '=', Utils::getLoginId())
            ->where(CourseClass::TABLE_NAME . '.' . CourseClass::STATUS, '=', CourseClass::STATUS_ACTIVE)
            ->get()
        ;

        $listCourse = [];
        foreach ($courses as $course){
            $totRev = ReviewModel::where(ReviewClass::ID_LECT_COURSE, $course->lecture_course_id)
                ->avg(ReviewClass::REVIEW);

            $obj=(object)[
                'course_name'=> $course->course_name,
                'id'=>$course->id,
                'lecture_course_id'=>$course->lecture_course_id,
                'image'=>$course->image,
                'desc'=>$course->desc,
                'title'=>$course->title,
                'name'=>$course->name,
                'surname'=>$course->surname,
                'review' => round($totRev),
            ];
            array_push($listCourse, $obj);
        }
//        return $listCourse;
        return view("front.course_list")
            ->with("courses", $listCourse)
            ;
    }

    public function ndiqKursin(Request $request){
        try {
            if (!empty($request->id)) {
                $id = htmlentities(trim($request->id));

                if (!Utils::isAttendingThis($id)){
                    $stud_course = new StudentCourseModel();
                    $stud_course->id_student = Utils::getUserId();
                    $stud_course->id_lecture_course = $id;
                    $stud_course->state = 1;
                    $stud_course->status = 1;
                    $stud_course->save();

                    return [
                        'status' => 1
                    ];
                }else{

                    return [
                        'status' => 0
                    ];
                }
            }

            return [
                'status' => 0
            ];
        }catch (Exception $e){
            return [
                'status' => 0
            ];
        }

    }
}
