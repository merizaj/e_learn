@extends("layouts.prime")
@section("title")
    UAMD-learn
@endsection

@section("styles")
@endsection

@section("content")
    <body id="page-top">

    <!-- PAGE WRAP -->
    <div id="page-wrap">

    @include("layouts.headerMenu")

    <!-- PROFILE FEATURE -->
        <section class="profile-feature">
            <div class="awe-parallax bg-profile-feature"></div>
            <div class="awe-overlay overlay-color-3"></div>
            <div class="container">
                <div class="info-author">
                    <div class="image">
                        <img src="images/user.svg" alt="">
                    </div>
                    <div class="name-author">
                        <h2 class="big">{!! \App\Http\Classes\Utils::getUsername() !!}</h2>
                    </div>
                    <div class="address-author">
                        <i class="fa fa-map-marker"></i>
                        <h3>Durres</h3>
                    </div>
                </div>
                <div class="info-follow">
                    <div class="trophies">
                        <span>12</span>
                        <p>Kurset</p>
                    </div>

                    <div class="trophies">
                        <span>20</span>
                        <p>Materialet</p>
                    </div>

                </div>
            </div>
        </section>
        <!-- END / PROFILE FEATURE -->

        <!-- CONTEN BAR -->
        @include("layouts.profile_minimenu")
        <!-- END / CONTENT BAR -->
        <!-- PROFILE -->
        <section class="profile">
            <div class="container">
                <h3 class="md black">Profili</h3>
                <div class="row">
                    <div class="col-md-9">
                        <div class="avatar-acount">
                            <div class="changes-avatar">
                                <div class="img-acount">
                                    <img src="images/user.svg" alt="">
                                </div>
                                <div class="choses-file up-file">
                                    <input type="file">
                                    <input type="hidden">
                                    <a href="#" class="mc-btn btn-style-6">Ndrysho foton</a>
                                </div>
                            </div>
                            <div class="info-acount">
                                <h3 class="md black">{!! \App\Http\Classes\Utils::getUsername() !!}</h3>

                                <div class="security">
                                    <div class="tittle-security">
                                        <h5>Email</h5>
                                        <input type="text">
                                        <h5>Fjalekalimi</h5>
                                        <input type="password" placeholder="Fjalekalimi aktual">
                                        <input type="password" placeholder="Fjalekalimi i ri">
                                        <input type="password" placeholder="Konfirmo fjalekalimin">
                                    </div>
                                </div>
                            </div>
                            <div class="input-save">
                                <input type="submit" value="Ruaj ndryshimet" class="mc-btn btn-style-1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="social-connect">
                            <h5>Kontaktet</h5>
                            <ul>
                                <li>
                                    <a href="#" class="twitter">
                                        <i class="icon md-twitter"></i>
                                        <p>https://www.facebook.com/</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="facebook">
                                        <i class="icon md-facebook-1"></i>
                                        <p>https://www.facebook.com/</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="gg-plus">
                                        <i class="icon md-google-plus"></i>
                                        <p>https://www.facebook.com/</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="printerest">
                                        <i class="icon md-pinterest-1"></i>
                                        <p>https://www.facebook.com/</p>
                                    </a>
                                </li>
                            </ul>
                            <div class="add-link">
                                <i class="icon md-plus"></i>
                                <input type="text" placeholder="paste link here">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / PROFILE -->

        @include("layouts.footerMenu")

    </div>
    <!-- END / PAGE WRAP -->

    @include("layouts.footerScripts")
    <script>
        $(document).ready(function(){

            $("#minimenu > li:nth-child(2)").addClass("current");

        });
    </script>
    </body>
@endsection
