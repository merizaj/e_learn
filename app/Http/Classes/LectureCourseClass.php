<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:04 PM
 */

namespace App\Http\Classes;


class LectureCourseClass
{
    const TABLE_NAME = "lecture_course";
    const ID = "id";
    const ID_LECTURE = "id_lecture";
    const ID_COURSE = "id_course";
    const TYPE = "type";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TYPE_SEMINAR = 1;
    const TYPE_LEXION = 2;
}