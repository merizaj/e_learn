@extends("layouts.prime")
@section("title")
    UAMD-learn
@endsection

@section("styles")
    <style>
        .unit_content {
            display: block;
        }

        .item_material_upload {
            display: none;
        }
        .open_body{
            border: 1px solid #fff;
            width: 22px;
            height: 22px;
            line-height: 20px;
            text-align: center;
        }
        .open_body i{
            width:20px;
        }
        .open_body i:before{
            color:white;
        }
    </style>
@endsection

@section("content")
    <body id="page-top">

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- PRELOADER -->
        <div id="preloader">
        </div>
        <!-- END / PRELOADER -->

    @include("layouts.headerMenu")

    <!-- BANNER CREATE COURSE -->
        <section class="sub-banner sub-banner-create-course">
            <div class="awe-parallax bg-profile-feature"></div>
            <div class="awe-overlay overlay-color-3"></div>
            <div class="container">
                <h2 class="md ilbl">{!! $course->course_name !!}</h2>
                <i class="icon md-pencil"></i>
            </div>
        </section>
        <!-- END / BANNER CREATE COURSE -->

        <!-- CREATE COURSE CONTENT -->
        <section id="create-course-section" class="create-course-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="create-course-sidebar">
                            <ul class="list-bar">
                                <li class="active">Dizajni i kursit</li>
                            </ul>
                            <div class="support">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-9">
                        <div class="create-course-content">

                            <!-- COURSE BANNER -->
                            <ul class="design-course-tabs" role="tablist">
                                <li class="active">
                                    <a href="#design-outline" role="tab" data-toggle="tab">
                                        <i class="icon md-list"></i>
                                        Pasqyra ( <span class="num_lessons">{!! $nr_lexione !!}</span> leksione)
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!-- DESIGN OUTLINE -->
                                <div class="tab-pane fade in active" id="design-outline">
                                    <div class="current-wrapper">
                                        <h4 class="sm black">Pasqyra aktuale</h4>
                                        <ul class="current-outline">
                                            <li><span class="num_lessons">{!! $nr_lexione !!}</span> leksione</li>
                                        </ul>
                                    </div>

                                {{--{{ Form::open(array('route' => 'course.save')) }}--}}
                                    <!-- SECTIONS -->
                                    <div class="dc-sections">
                                        <!-- DC SECTION INFO -->
                                        <div class="dc-section-info">

                                        <!-- DC SECTION BODY -->
                                            <div class="dc-section-body" id="section_1">

                                                @foreach($lexion as $l)

                                                <!-- DC UNIT -->
                                                <div class="dc-unit-info dc-course-item">
                                                    <div class="title-section">
                                                        <h5 class="xsm black">
                                                            <span class="lesson_title" contenteditable="true">{!! $l["title"] !!}</span>
                                                        </h5>
                                                        <input type="hidden" class="title_holder" name="title"
                                                               value="{!! $l["title"] !!}">
                                                        <input type="hidden" name="lesson_id" class="lesson_id"
                                                               value="{!! $l["id"] !!}">
                                                        <div class="course-region-tool">
                                                            <span
                                                               class="toggle-section active open_body"><i
                                                                        class="fa fa-caret-right "></i></span>
                                                            <a href="#" class="delete" title="delete"><i
                                                                        class="icon md-recycle"></i></a>
                                                            <span class="save"><i class="fa fa-save"
                                                                                  style="color: white;
                                                                                  margin-left: 12px;"></i>
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div class="unit-body dc-item-body unit_content">
                                                        <table class="tb-course">
                                                            <tbody class="item_body">

                                                            <tr class="tb-upload-content">
                                                                <td class="label-info">
                                                                    <label for="">Material</label>
                                                                </td>
                                                                <td class="td-form-item">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="upload-drag">
                                                                                <h5 class="sm black">Upload file
                                                                                    Here</h5>
                                                                                {{--<span>Drag to upload Your file from computer</span>--}}
                                                                                <p class="or">Or</p>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="paste-link">
                                                                                <h5 class="sm black">Paste Link
                                                                                    Video</h5>

                                                                                <input type="text">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>


                                                            <tr class="tb-upload-content">
                                                                <td class="label-info">
                                                                    <label for="">Content</label>
                                                                </td>
                                                                <td class="td-form-item">
                                                                    <div class="content-item-info">

                                                                        <div class="uploading upload-info text-center tb">
                                                                            <div class="add-thumb-wrap tb-cell">
                                                                                <a href="#" class="add-thumb">
                                                                                    <i class="icon md-plus"></i>
                                                                                    Add Thumbnail
                                                                                </a>
                                                                            </div>
                                                                            <div class="tb-cell">
                                                                                <div class="title-file">
                                                                                    <span class="name-file">Name.ext</span>
                                                                                    <span class="size-file">( 12mb )</span>
                                                                                    <a href="#" class="close-file">
                                                                                        <i class="icon md-close-2"></i>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="upload-meta">
                                                                                    <span>Uploaded by Admin . 30, May,2014</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                            @for($i = 0; $i< count($l["item"]); $i++)

                                                            <tr class="tr-file doc_input">
                                                                <td class="label-info">
                                                                    <label for="">Material</label>
                                                                </td>
                                                                <td class="td-form-item" style="width:340px">
                                                                    <div class="form-item">
                                                                        <input type="text" class="item_material"
                                                                               value="{!! $l["item"][$i]->title !!}"
                                                                               name="item_doc_name[]"/>

                                                                        <input type="hidden" class="item_material_id"
                                                                               value="{!! $l["item"][$i]->id !!}" name="item_doc_id[]"/>
                                                                    </div>
                                                                </td>
                                                                <td class="td-form-item">
                                                                    <div class="form-item">
                                                                        <input type="file" class="item_material_upload"
                                                                               name="item"
                                                                               style="margin-top: 0"
                                                                               accept="application/vnd.ms-excel,
                                                                                        application/msword,
                                                                                        .pdf,
                                                                                        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">

                                                                    </div>
                                                                    <input type="hidden" class="item_doc_path" name="item_doc_path[]"
                                                                           value="{!! $l["item"][$i]->path !!}">
                                                                </td>
                                                            </tr>
                                                            @endfor

                                                            <tr class="tr-file doc_input">
                                                                <td class="label-info">
                                                                    <label for="">Material</label>
                                                                </td>
                                                                <td class="td-form-item" style="width:340px">
                                                                    <div class="form-item">
                                                                        <input type="text" class="item_material"
                                                                               value=""
                                                                               name="item_doc_name[]"/>

                                                                        <input type="hidden" class="item_material_id"
                                                                               value="" name="item_doc_id[]"/>
                                                                    </div>
                                                                </td>
                                                                <td class="td-form-item">
                                                                    <div class="form-item">
                                                                        <input type="file" class="item_material_upload"
                                                                               name="item"
                                                                               style="margin-top: 0"
                                                                               accept="application/vnd.ms-excel,
                                                                                        application/msword,
                                                                                        .pdf,
                                                                                        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">

                                                                    </div>
                                                                    <input type="hidden" class="item_doc_path" name="item_doc_path[]"
                                                                           value="">
                                                                </td>
                                                            </tr>
                                                            <tr class="shto_btn_parent">
                                                                <td></td>
                                                                <td>
                                                                    <button type="button"
                                                                            class=" mc-btn-3 btn-style-1
                                                                        popup-with-zoom-anim
                                                                        shto_doc_btn">
                                                                        <i class="icon md-plus"></i> Shto Dokument
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                                <!-- END / DC UNIT -->
                                                @endforeach
                                            </div>
                                            <!-- END / DC SECTION BODY -->

                                        </div>
                                        <!-- END / DC SECTION INFO -->

                                        <!-- BUTTON ADD AND POPUP SECTION -->
                                        <div class="add-section">
                                            <button type="button" class="mc-btn-3 btn-style-1 popup-with-zoom-anim add_lexion">
                                                <i class="icon md-plus"></i> Shto Leksion</button>

                                        </div>
                                        <!-- END / BUTTON ADD AND POPUP SECTION -->

                                    </div>
                                    <!-- END / SECTIONS -->
{{--                                    {{ Form::close() }}--}}

                                </div>
                                <!-- END / DESIGN OUTLINE -->

                                <!-- DESIGN QUESTION -->
                                <div class="tab-pane fade" id="design-question">
                                    <div class="total-quest">
                                        <h4 class="sm black">Total Questions</h4>
                                        <div class="count"><span>5</span> questions</div>
                                        <div class="new-question">
                                            <a href="#" class="mc-btn-3 btn-style-7">
                                                <i class="icon md-plus"></i>New question
                                            </a>
                                        </div>
                                    </div>
                                    <table class="table-quest">
                                        <thead>
                                        <tr>
                                            <th>Title <span class="caret"></span></th>
                                            <th>Type <span class="caret"></span></th>
                                            <th>Description</th>
                                            <td class="course-region-tool">

                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td>Anna Molly 268</td>
                                            <td>Multiple Choice</td>
                                            <td>Lorem ipsum dolor sit amet, consectetur</td>
                                            <td class="course-region-tool">
                                                <a href="#"><i class="icon md-pencil"></i></a>
                                                <a href="#"><i class="icon md-recycle"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Anna Molly 268</td>
                                            <td>Multiple Choice</td>
                                            <td>Lorem ipsum dolor sit amet, consectetur</td>
                                            <td class="course-region-tool">
                                                <a href="#"><i class="icon md-pencil"></i></a>
                                                <a href="#"><i class="icon md-recycle"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Anna Molly 268</td>
                                            <td>Multiple Choice</td>
                                            <td>Lorem ipsum dolor sit amet, consectetur</td>
                                            <td class="course-region-tool">
                                                <a href="#"><i class="icon md-pencil"></i></a>
                                                <a href="#"><i class="icon md-recycle"></i></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END / DESIGN QUESTION -->

                                <!-- DESIGN ASSIGNMENT -->
                                <div class="tab-pane fade" id="design-assignment">
                                    <div class="total-assignment">
                                        <h4 class="sm black">Total Assignments</h4>
                                        <div class="count"><span>3</span> assignments</div>
                                        <div class="new-question">
                                            <a href="#" class="mc-btn-3 btn-style-7">
                                                <i class="icon md-plus"></i>New Assignment
                                            </a>
                                        </div>
                                    </div>
                                    <table class="table-assignment">
                                        <thead>
                                        <tr>
                                            <th class="count"></th>
                                            <th>Title</th>
                                            <td class="course-region-tool">

                                        </tr>
                                        </thead>

                                        <tbody>
                                        <tr>
                                            <td class="count">1</td>
                                            <td>Lorem ipsum dolor sit amet, consectetur</td>
                                            <td class="course-region-tool">
                                                <a href="#"><i class="icon md-pencil"></i></a>
                                                <a href="#"><i class="fa fa-arrows"></i></a>
                                                <a href="#"><i class="icon md-recycle"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="count">2</td>
                                            <td>Lorem ipsum dolor sit amet, consectetur</td>
                                            <td class="course-region-tool">
                                                <a href="#"><i class="icon md-pencil"></i></a>
                                                <a href="#"><i class="fa fa-arrows"></i></a>
                                                <a href="#"><i class="icon md-recycle"></i></a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="count">3</td>
                                            <td>Lorem ipsum dolor sit amet, consectetur</td>
                                            <td class="course-region-tool">
                                                <a href="#"><i class="icon md-pencil"></i></a>
                                                <a href="#"><i class="fa fa-arrows"></i></a>
                                                <a href="#"><i class="icon md-recycle"></i></a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END / DESIGN ASSIGNMENT -->


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / CREATE COURSE CONTENT -->


        @include("layouts.footerMenu")

    </div>
    <!-- END / PAGE WRAP -->

    @include("layouts.footerScripts")
    <script>
        $(document).ready(function () {

            var lesson_number = parseInt('{!! $nr_lexione !!}') ;
            $(".num_lessons").html(lesson_number);

            console.log(lesson_number);

            //region variables_html
            var lexion_body_html = '<div class="dc-unit-info dc-course-item new ">\n' +
                '                         <div class="title-section">\n' +
                '                              <h5 class="xsm black">\n' +
                '                               <span class="lesson_title" contenteditable="true"> Titulli</span>\n' +
                '                           </h5>\n' +
                '                           <input type="hidden" class="title_holder" name="title" value="0">\n' +
                '                           <input type="hidden" name="lesson_id" class="lesson_id" value="0">\n' +
                '                             <div class="course-region-tool">\n' +
                '                                 <span class="toggle-section active open_body">' +
                '<i class="fa fa-caret-right "></i></span>' +
                '                                 <a href="#" class="delete" title="delete"><i\n' +
                '                                             class="icon md-recycle"></i></a>\n' +
                '                                   <span class="save"><i class="fa fa-save"\n' +
                '                                                        style="color: white;margin-left: 12px;"></i></span>\n' +
                '                                               </div>\n' +
                '                                           </div>\n' +
                '\n' +        '                                           <div class="unit-body dc-item-body unit_content">\n' +
                '                                               <table class="tb-course">\n' +
                '                                                   <tbody class="item_body">\n' +
                '\n' +        '                                                   <tr class="tb-upload-content">\n' +
                '                                                  <td class="label-info">\n' +
                '                                                               <label for="">Material</label>\n' +
                '                                                             </td>\n' +
                '                                                             <td class="td-form-item">\n' +
                '                                                              <div class="row">\n' +
                '                                                              <div class="col-md-6">\n' +
                '                                                                        <div class="upload-drag">\n' +
                '                                                                           <h5 class="sm black">Upload file\n' +
                '                                                                            Here</h5>\n' +
                '                                                                          {{--<span>Drag to upload Your file from computer</span>--}}\n' +
                '                                                                             <p class="or">Or</p>\n' +
                '                                                                           </div>\n' +
                '                                                                      </div>\n' +
                '\n' +                '                                                                        <div class="col-md-6">\n' +
                '                                                                        <div class="paste-link">\n' +
                '                                                                            <h5 class="sm black">Paste Link\n' +
                '                                                                                 Video</h5>\n' +
                '                                                                          {{--<span>Youtube or Vimeo only</span>--}}\n' +
                '                                                                          <input type="text">\n' +
                '                                                                         </div>\n' +
                '                                                                        </div>\n' +
                '                                                                    </div>\n' +
                '                                                                </td>\n' +
                '                                                            </tr>\n' +
                '\n' +
                '\n' +
                '                                                            <tr class="tb-upload-content">\n' +
                '                                                                <td class="label-info">\n' +
                '                                                                    <label for="">Content</label>\n' +
                '                                                                </td>\n' +
                '                                                                <td class="td-form-item">\n' +
                '                                                                    <div class="content-item-info">\n' +
                '                                                                        <div class="uploading upload-info text-center tb">\n' +
                '                                                                            <div class="add-thumb-wrap tb-cell">\n' +
                '                                                                                <a href="#" class="add-thumb">\n' +
                '                                                                                    <i class="icon md-plus"></i>\n' +
                '                                                                                    Add Thumbnail\n' +
                '                                                                                </a>\n' +
                '                                                                            </div>\n' +
                '                                                                            <div class="tb-cell">\n' +
                '                                                                                <div class="title-file">\n' +
                '                                                                                    <span class="name-file">Name.ext</span>\n' +
                '                                                                                    <span class="size-file">( 12mb )</span>\n' +
                '                                                                                    <a href="#" class="close-file">\n' +
                '                                                                                        <i class="icon md-close-2"></i>\n' +
                '                                                                                    </a>\n' +
                '                                                                                </div>\n' +
                '                                                                                <div class="upload-meta">\n' +
                '                                                                                    <span>Uploaded by Admin . 30, May,2014</span>\n' +
                '                                                                                </div>\n' +
                '                                                                            </div>\n' +
                '                                                                        </div>\n' +
                '                                                                    </div>\n' +
                '                                                                </td>\n' +
                '                                                            </tr>\n' +
                '\n' +
                '\n' +
                '                                                            <tr class="tr-file doc_input">\n' +
                '                                                                <td class="label-info">\n' +
                '                                                                    <label for="">Material</label>\n' +
                '                                                                </td>\n' +
                '                                                                <td class="td-form-item" style="width:340px">\n' +
                '                                                                    <div class="form-item">\n' +
                '                                                                        <input type="text" class="item_material"\n' +
                '                                                                               value="" name="item_doc_name[]"/>' +
                '<input type="hidden" class="item_material_id" value="" name="item_doc_id[]"/>\n' +
                '                                                                    </div>\n' +
                '                                                                </td>\n' +
                '                                                                <td class="td-form-item">\n' +
                '                                                                    <div class="form-item">\n' +
                '                                                                        <input type="file" class="item_material_upload"\n' +
                '                                                                               name="item"\n' +
                '                                                                               style="margin-top: 0"\n' +
                '                                                                               accept="application/vnd.ms-excel,' +
                'application/msword, \n' +
                '                                                                                        application/pdf,\n' +
                '                                                                                        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">\n' +
                '                                                                        <input type="hidden" class="item_doc_path" name="item_doc_path[]"\n' +
                '                                                                               value="">\n' +
                '                                                                    </div>\n' +
                '                                                                </td>\n' +
                '                                                            </tr>\n' +
                '\n' +
                '                                                            <tr class="shto_btn_parent">\n' +
                '                                                                <td></td>\n' +
                '                                                                <td>\n' +
                '                                                                    <button type="button" ' +
                '                                                                            class=" mc-btn-3 btn-style-1\n' +
                '                                                                        popup-with-zoom-anim\n' +
                '                                                                        shto_doc_btn">\n' +
                '                                                                        <i class="icon md-plus"></i> Shto Dokument\n' +
                '                                                                    </button>\n' +
                '                                                                </td>\n' +
                '                                                            </tr>\n' +
                '                                                            </tbody>\n' +
                '                                                        </table>\n' +
                '\n' +
                '                                                    </div>\n' +
                '                                                </div>';

            var doc_upload_html = '<tr class="tr-file doc_input">\n' +
                '                        <td class="label-info">\n' +
                '                            <label for="">Material</label>\n' +
                '                        </td>\n' +
                '                        <td class="td-form-item">\n' +
                '                            <div class="form-item">\n' +
                '                                <input type="text" class="item_material"\n' +
                '                                       value="" name="item_doc_name[]"/>\n' +
                '                            </div>\n' +
                '                        </td>\n' +
                '                        <td class="td-form-item">\n' +
                '                            <div class="form-item">\n' +
                '                                <input type="file" class="item_material_upload"\n' +
                '                                       name="item"\n' +
                '                                       style="margin-top: 0"\n' +
                '                                       accept="application/vnd.ms-excel,application/msword, \n' +
                '                                                .pdf,\n' +
                '                                                application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">\n' +
                '                                '+
                '                            </div> <input type="hidden" class="item_doc_path" name="item_doc_path[]"   value="">\n' +
                '                        </td>' +
                '<td class="td-form-item"><i class="icon md-close-2 remove_upload_item" style="color: red;"></i></td>\n' +
                '                    </tr>\n' +
                '\n' +
                '                    <tr class="shto_btn_parent">\n' +
                '                        <td></td>\n' +
                '                        <td>\n' +
                '                            <button type="button" class=" mc-btn-3 btn-style-1\n' +
                '                                popup-with-zoom-anim\n' +
                '                                shto_doc_btn">\n' +
                '                                <i class="icon md-plus"></i> Shto Dokument\n' +
                '                            </button>\n' +
                '                        </td>' +
                '                    </tr>';

            //endregion


            open_body_click();
            item_doc_upload_events();
            save_lesson();
            add_lexion_click();
            remove_lexion_click();

            function open_body_click(){
                console.log("info:", "open body clicked");

                $(".open_body").off("click");
                $(".open_body").on("click", function () {

                    var arrow = $(this);
                    if (!arrow.hasClass("active")) {
                        console.log("open");
                        arrow.parents(".dc-course-item").find(".unit_content").show();
                        arrow.addClass("active");
                    } else {
                        console.log("close");
                        arrow.parents(".dc-course-item").find(".unit_content").hide();
                        arrow.removeClass("active");
                    }
                });
            }

            function item_doc_upload_events() {
                console.log("upload events");

                $(".item_material_upload").change(function () {
                    console.log("CHANGE");

                    var upload_btn = $(this);
                    var file = $(this)[0].files[0];
                    var filename = file.name;
                    var formData = new FormData();
                    formData.append('document', file, filename);
                    formData.append('id', '{!! $course->lecture_course_id !!}');
                    formData.append('_token', '{!! csrf_token() !!}');
                    console.log(file.name);
                    $.ajax({
                        url: '{!! route("course.upload") !!}',
                        type: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: formData,
                        success: function (data) {
                            if (data.sts === 1) {
                                upload_btn.parents(".doc_input").find(".item_material").val(file.name);
                                toastr.success(data.msg);
                                console.log(data.path);
                                upload_btn.parents(".doc_input").find('.item_doc_path').val(data.path);
                                add_btn_click();
                            } else {
                                toastr.error(data.msg);
                            }
                        }
                    });
                });
            }

            function add_btn_click(){
                console.log("shto btn click event");
                $(".shto_doc_btn").off('click');
                $(".shto_doc_btn").on('click',function () {
                    console.log("shto btn clicked");
                    $(this).parents(".item_body:last-child").append(doc_upload_html);
                    console.log("shto btn Appended html");

                    $(this).parent().find(".doc_input").find(".remove_upload_item").click(function () {
                        console.log("remove item doocument");
                        $(this).parents(".doc_input").html("");
                    });
                    $(this).parents(".shto_btn_parent").remove();
                    item_doc_upload_events();
//                    add_btn_click();
                });


            }

            function add_lexion_click() {
                console.log("info:", "add lexion clicked");

                $(".add_lexion").click(function () {

                    $("#section_1").append(lexion_body_html);
//                    $(".new").find(".lex_number").text(lesson_number);
                    $(".num_lessons").html(lesson_number);
                    lesson_number +=1;
                    open_body_click();
                    item_doc_upload_events();
                    save_lesson();
//                    console.log($(lexion_body_html).find(".lex_number"));
                });

            }

            function save_lesson(){
                $(".save").off('click');
                $(".save").on('click', function () {

                    var parent = $(this).parents(".dc-course-item");
                    var title_lesson = $(this).parents(".dc-course-item").find(".lesson_title").html();
                    var lesson_id = $(this).parents(".dc-course-item").find(".lesson_id");
                    $(this).parents(".dc-course-item").find('input[name=title]').val(title_lesson);
                    var item_doc_name_length = $(this).parents(".dc-course-item").find('input[name="item_doc_name[]"]').length;
                    var item_doc_name =  [];
                    var item_doc_path_length = $(this).parents(".dc-course-item").find('input[name="item_doc_path[]"]').length;
                    var item_doc_path = [];
                    var item_doc_id_length = $(this).parents(".dc-course-item").find('input[name="item_doc_id[]"]').length;
                    var item_doc_id = [];
                    for (var i = 0; i< item_doc_name_length; i++){
                        item_doc_name.push(($(this).parents(".dc-course-item").find('input[name="item_doc_name[]"]')[i]).value)
                    }
                    for (var j = 0; j< item_doc_path_length; j++){
                        item_doc_path.push(($(this).parents(".dc-course-item").find('input[name="item_doc_path[]"]')[j]).value)
                    }
                    for (var l = 0; l< item_doc_id_length; l++){
                        item_doc_id.push(($(this).parents(".dc-course-item").find('input[name="item_doc_id[]"]')[l]).value)
                    }

                    var formData = {
                        'item_doc_name'              : item_doc_name,
                        'item_doc_path'              : item_doc_path,
                        'item_doc_id'              : item_doc_id,
                        'title'             : $(this).parents(".dc-course-item").find('input[name=title]').val(),
                        'lesson_id'    : $(this).parents(".dc-course-item").find('input[name=lesson_id]').val(),
                        'id'    : '{!!  $course->lecture_course_id !!}',
                        '_token'    : '{!! csrf_token() !!}'
                    };

                    console.log(formData);

                    // process the form
                    $.ajax({
                        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url         : '{!! route("course.save") !!}', // the url where we want to POST
                        data        : formData, // our data object
                        dataType    : 'json', // what type of data do we expect back from the server
                        encode      : true
                    })
                    // using the done promise callback
                        .done(function(data) {

                            console.log(data);

                            if(data.status === 1){

                                lesson_id.val(data.lesson_id);
                                toastr.success(data.msg);
                                parent.find(".open_body").trigger("click");
                            }
                        });
                });
            }
            function remove_lexion_click() {
                $(".delete").off('click');
                $(".delete").on('click', function () {

                   var leksion_body = $(this).parents(".dc-course-item");
                   var id = $(this).parents(".dc-course-item").find(".lesson_id").val();
                    // alert(id);
                    swal({
                        title: 'A jeni te sigurte',
                        text: 'Ky Leksoin do te fshihet perfundimisht!',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Po, fshije",
                        cancelButtonText: "Anullo",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    },
                    function() {
                        $.ajax({
                            url: '{!! route('course.delete') !!}',
                            type: "POST",
                            data: {"_token": '{!! csrf_token() !!}', id: id},
                            success: function (data) {
                                if (data.sts == 1) {
                                    console.log(data.msg);
                                    swal({
                                            title: "Kursi u fshi!",
                                            type: "success",
                                            closeOnConfirm: true
                                        },
                                        function () {
                                        leksion_body.remove();
                                            lesson_number -= 1;
                                            $(".num_lessons").html(lesson_number);
                                            {{--window.location.href = '{!! route("view_software") !!}'--}}
                                        });
                                } else {
                                    swal(data.error);
                                }
                            }
                        })
                    });
                })
            }

        });
    </script>
    </body>
@endsection