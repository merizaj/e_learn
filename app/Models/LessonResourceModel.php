<?php

namespace App\Models;

use App\Http\Classes\LessonResourceClass;
use Illuminate\Database\Eloquent\Model;

class LessonResourceModel extends Model
{
    protected $table = LessonResourceClass::TABLE_NAME;
    protected $primaryKey = LessonResourceClass::ID;
    public $timestamps = true;
}
