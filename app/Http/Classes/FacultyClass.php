<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 4:11 PM
 */

namespace App\Http\Classes;


class FacultyClass
{
    const TABLE_NAME = "faculty";
    const ID = "id";
    const NAME = "name";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}