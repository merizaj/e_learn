<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:03 PM
 */

namespace App\Http\Classes;


class LoginClass
{

    const TABLE_NAME = "login";
    const ID = "id";
    const UNAME = "uname";
    const UPASS = "upass";
    const ROLE = "role";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const ADMIN = 1;
    const LECTURER = 2;
    const STUDENT = 3;

    public function isStudent()
    {
        return session(self::ROLE) == LoginClass::STUDENT;
    }
    public function isLecturer()
    {
        return session(self::ROLE) == LoginClass::STUDENT;

    }
}