<?php

use App\Http\Classes\LessonClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LessonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LessonClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(LessonClass::ID);
            $table->string(LessonClass::ID_LECTURE_COURSE);
            $table->string(LessonClass::TITLE);
            $table->string(LessonClass::DESC)->nullable();
            $table->string(LessonClass::DURATION)->nullable();
            $table->smallInteger(LessonClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
