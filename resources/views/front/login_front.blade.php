@extends("layouts.prime")
@section("title")
    Login
@endsection

@section("styles")
@endsection

@section("content")
    <body id="page-top" class="home">

    <!-- PAGE WRAP -->
    <div id="page-wrap">

    @include("layouts.headerMenu")

    <!-- LOGIN -->
        <section id="login-content" class="login-content">
            <div class="awe-parallax bg-login-content"></div>
            <div class="awe-overlay"></div>
            <div class="container">
                <div class="row">

                    <!-- FORM -->
                    <div class="col-xs-12 col-lg-4 pull-right">
                        <div class="form-login">
                            {{ Form::open(array('route' => 'login_confirm')) }}
                            {{--<form>--}}
                            <h2 class="text-uppercase">Hyr</h2>
                            <div class="form-email">
                                @if ($errors->any())
                                    <div class="alert alert-danger" style="padding: 12px">
                                        <ul style="list-style: none">
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                            <div class="form-email">
                                <input type="text" name="username" placeholder="Email">
                            </div>
                            <div class="form-password">
                                <input type="password" name="password" placeholder="Fjalekalim">
                            </div>
                            <div class="form-check">
                                <input type="checkbox" id="check">
                                <label for="check">
                                    <i class="icon md-check-2"></i>
                                    Mbaj mend</label>
                                <a href="#">Harruat fjalekalimini?</a>
                            </div>
                            <div class="form-submit-1">
                                <input type="submit" value="Hyr" class="mc-btn btn-style-1">
                            </div>

                            {{--</form>--}}
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- END / FORM -->

                    <div class="image">
                        <img src="images/homeslider/img-thumb.png" alt="">
                    </div>

                </div>
            </div>
        </section>
        <!-- END / LOGIN -->

        @include("layouts.footerMenu")

    </div>
    <!-- END / PAGE WRAP -->

    @include("layouts.footerScripts")
    </body>
@endsection
