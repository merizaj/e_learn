<?php

use App\Http\Classes\LessonResourceClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LessonResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LessonResourceClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(LessonResourceClass::ID);
            $table->string(LessonResourceClass::TITLE);
            $table->string(LessonResourceClass::LINK)->nullable();
            $table->string(LessonResourceClass::PATH);
            $table->string(LessonResourceClass::ID_LECTURE_COURSE);
            $table->smallInteger(LessonResourceClass::TYPE);
            $table->smallInteger(LessonResourceClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
