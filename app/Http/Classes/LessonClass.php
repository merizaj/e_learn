<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:05 PM
 */

namespace App\Http\Classes;


class LessonClass
{
    const TABLE_NAME = "lesson";
    const ID = "id";
    const TITLE = "title";
    const DESC = "desc";
    const DURATION = "duration";
    const ID_LECTURE_COURSE = "id_lect_course";
    const STATUS = "status";

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
}