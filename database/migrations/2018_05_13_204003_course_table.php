<?php

use App\Http\Classes\CourseClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CourseClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(CourseClass::ID);
            $table->string(CourseClass::NAME);
            $table->string(CourseClass::DESC)->nullable();
            $table->string(CourseClass::IMAGE)->nullable();
            $table->string(CourseClass::DURATION)->nullable();
            $table->integer(CourseClass::VITI)->nullable();
            $table->string(CourseClass::START)->nullable();
//            $table->string(CourseClass::OBJECTIVES)->nullable();
            $table->integer(CourseClass::ID_DEPARTMENT);
            $table->smallInteger(CourseClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
