<?php

use App\Http\Classes\FacultyClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FacultyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FacultyClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(FacultyClass::ID);
            $table->string(FacultyClass::NAME);
            $table->smallInteger(FacultyClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
