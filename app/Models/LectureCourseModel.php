<?php

namespace App\Models;

use App\Http\Classes\LectureCourseClass;
use Illuminate\Database\Eloquent\Model;

class LectureCourseModel extends Model
{
    protected $table = LectureCourseClass::TABLE_NAME;
    protected $primaryKey = LectureCourseClass::ID;
    public $timestamps = true;
}
