<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:04 PM
 */

namespace App\Http\Classes;


class StudentCourseClass
{
    const TABLE_NAME = "student_course";
    const ID = "id";
    const ID_STUDENT = "id_student";
    const ID_LECTURE_COURSE = "id_lecture_course";
    const STATE = "state";
    const STATUS = "status";
}