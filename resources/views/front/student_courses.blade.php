@extends("layouts.prime")
@section("title")
    UAMD-learn
@endsection

@section("styles")
@endsection

@section("content")
    <body id="page-top">
    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- PRELOADER -->
        <div id="preloader">
            <div class="pre-icon">
                <div class="pre-item pre-item-1"></div>
                <div class="pre-item pre-item-2"></div>
                <div class="pre-item pre-item-3"></div>
                <div class="pre-item pre-item-4"></div>
            </div>
        </div>
        <!-- END / PRELOADER -->

    @include("layouts.headerMenu")

        <!-- PROFILE FEATURE -->
            <section class="profile-feature">
            <div class="awe-parallax bg-profile-feature"></div>
            <div class="awe-overlay overlay-color-3"></div>
            <div class="container">
                <div class="info-author">
                    <div class="image">
                        <img src="images/user.svg" alt="">
                    </div>
                    <div class="name-author">
                        <h2 class="big">{!! \App\Http\Classes\Utils::getUsername() !!}</h2>
                    </div>
                    <div class="address-author">
                        <i class="fa fa-map-marker"></i>
                        <h3>Durrës</h3>
                    </div>
                </div>
                <div class="info-follow">
                    <div class="trophies">
                        <span>12</span>
                        <p>Kurset</p>
                    </div>
                    <div class="trophies">
                        <span>20</span>
                        <p>Materialet</p>

            </div>
        </section>
        <!-- END / PROFILE FEATURE -->

        <!-- CONTEN BAR -->
            @include("layouts.profile_minimenu")
        <!-- END / CONTENT BAR -->

        <!-- COURSE CONCERN -->
            <section id="course-concern" class="course-concern">
            <div class="container">
                <div class="row">
                    @foreach($courses as $course)
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <!-- MC ITEM -->
                        <div class="mc-learning-item mc-item mc-item-2">
                            <div class="image-heading">
                                <img src="{!! $course->image !!}" alt="">
                            </div>
                            <div class="meta-categories">
                                @if(\App\Http\Classes\Utils::isLecturer())
                                <a href="{!! \Illuminate\Support\Facades\URL::route("course_intro",
                                                        $course->lecture_course_id) !!}">
                                    {!! $course->course_name !!}</a>
                                @else
                                    <a href="{!! \Illuminate\Support\Facades\URL::route("course_intro",
                                                    $course->lecture_course_id) !!}">
                                        {!! $course->course_name !!}</a>
                                @endif

                            </div>
                            <div class="content-item">
                                <div class="image-author">
                                    <img src="{!! $course->image !!}" alt="">
                                </div>
                                <h4>
                                    @if(\App\Http\Classes\Utils::isLecturer())
                                        <a href="{!! \Illuminate\Support\Facades\URL::route("course_intro",
                                                        $course->lecture_course_id) !!}">
                                            {!! $course->course_name !!}</a>
                                    @else
                                        <a href="{!! \Illuminate\Support\Facades\URL::route("course_intro",
                                                    $course->lecture_course_id) !!}">
                                            {!! $course->course_name !!}</a>
                                    @endif
                                </h4>
                                <div class="name-author">
                                    <a href="#">
                                        {!! $course->title !!}
                                        {!! $course->name  !!}
                                        {!! $course->surname !!}
                                        </a>
                                </div>
                            </div>
                            <div class="ft-item">
                                <div class="percent-learn-bar">
                                    <div class="percent-learn-run"></div>
                                </div>
                                <div class="percent-learn">100%<i class="fa fa-trophy"></i></div>
                                <a href="#" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a>
                            </div>
                        </div>
                        <!-- END / MC ITEM -->
                    </div>
                    @endforeach

                </div>
            </div>
        </section>
        <!-- END / COURSE CONCERN -->

            @include("layouts.footerMenu")
    </div>
    <!-- END / PAGE WRAP -->
    @include("layouts.footerScripts")
    <script>
        $(document).ready(function(){

            $("#minimenu > li:nth-child(1)").addClass("current");

        });
    </script>
    </body>
@endsection