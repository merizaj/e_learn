<?php

use App\Http\Classes\LecturerClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LecturerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LecturerClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(LecturerClass::ID);
            $table->string(LecturerClass::NAME);
            $table->string(LecturerClass::SURNAME);
            $table->string(LecturerClass::TITLE);
            $table->string(LecturerClass::EMAIL)->nullable();
            $table->string(LecturerClass::CEL)->nullable();
            $table->string(LecturerClass::ID_LOGIN);
            $table->string(LecturerClass::ID_DEPARTMENT);
            $table->smallInteger(LecturerClass::IS_DEP_HEAD)->default('1');
            $table->smallInteger(LecturerClass::STATUS)->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
