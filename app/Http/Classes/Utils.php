<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/17/2018
 * Time: 7:25 PM
 */

namespace App\Http\Classes;


use App\Models\LecturerModel;
use App\Models\StudentCourseModel;
use App\Models\StudentModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class Utils
{

    const SESSION_ROLE = 'session_role';
    const SESSION_LOGIN_ID = 'session_id';
    const SESSION_USERNAME = 'session_uname';

    public static function isLoggedIn()
    {
        if (session(self::SESSION_LOGIN_ID) != null) {

            return true;
        } else {
            return false;
        }
    }


    public static function encrypt($string)
    {
        return Hash::make($string);
    }

    public static function setLogin($user_id, $role, $username)
    {

        session([self::SESSION_LOGIN_ID => $user_id]);
        session([self::SESSION_ROLE => $role]);
        session([self::SESSION_USERNAME => $username]);
    }

    public static function unsetLogin()
    {
        Session::forget(self::SESSION_LOGIN_ID);
        Session::forget(self::SESSION_USERNAME);
        Session::forget(self::SESSION_ROLE);
    }

    public static function getUsername()
    {
        return \session(self::SESSION_USERNAME);
    }

    public static function isStudent()
    {
        return session(self::SESSION_ROLE) == LoginClass::STUDENT;
    }
    public static function getRole()
    {
        return session(self::SESSION_ROLE);
    }

    public static function isLecturer()
    {
        return session(self::SESSION_ROLE) == LoginClass::LECTURER;
    }

    public static function getUserId()
    {
        if (self::isStudent()){
            $student = StudentModel::where(StudentClass::TABLE_NAME.'.'.StudentClass::ID_LOGIN, self::getLoginId())
                ->first();

            if ($student){
                return $student->id;

            }else{

                return 0;
            }
        }else{
            $lect = LecturerModel::where(LecturerClass::TABLE_NAME.'.'.LecturerClass::ID_LOGIN, self::getLoginId())
                ->first();

            if ($lect){
                return $lect->id;
            }else{
                return 0;
            }
        }
    }

    public static function getLoginId()
    {
        return session(self::SESSION_LOGIN_ID);

    }

    public static function isAttendingThis($id)
    {
        if (isset($id)){
            $id = htmlentities(trim($id));

            $course = StudentCourseModel::where(StudentCourseClass::TABLE_NAME.'.'.StudentCourseClass::ID_LECTURE_COURSE, $id )
                ->where(StudentCourseClass::ID_STUDENT, Utils::getUserId())
                ->first();
            if ($course){
                return true;
            }

            return false;
        }
    }
}