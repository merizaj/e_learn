<?php
/**
 * Created by PhpStorm.
 * User: donald
 * Date: 5/13/2018
 * Time: 3:04 PM
 */

namespace App\Http\Classes;


class LecturerClass
{
    const TABLE_NAME = "lecturer";
    const ID = "id";
    const NAME = "name";
    const SURNAME = "surname";
    const TITLE = "title";
    const EMAIL = "email";
    const CEL = "cel";
    const ID_DEPARTMENT = "id_department";
    const ID_LOGIN = "id_login";
    const IS_DEP_HEAD = "dep_head";
    const STATUS = "status";

    const DEPHEAD_TRUE = 1;
    const DEPHEAD_FALSE = 0;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;


}