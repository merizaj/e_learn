<?php

use App\Http\Classes\StudentCourseClass;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StudentCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(StudentCourseClass::TABLE_NAME, function (Blueprint $table) {
            $table->increments(StudentCourseClass::ID);
            $table->integer(StudentCourseClass::ID_STUDENT);
            $table->integer(StudentCourseClass::ID_LECTURE_COURSE);
            $table->smallInteger(StudentCourseClass::STATE)->nullable();
            $table->smallInteger(StudentCourseClass::STATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
